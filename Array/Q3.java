// Given an array of size N Return the count of pairs (i,j) with arr[i]+arr[j]==k.
// Input: N= 10 
//       arr={3,5,2,1,-3,7,8,15,6,13}
//       k=10
//   note i!=j
// Output: 6 pairs

 import java.util.*;
 class Q3 { 
	static int pair(int [] arr, int k ) {
		int count =0;
	       for(int i=0; i<arr.length;i++){
	  		       for (int j=0; j<arr.length; j++){
				   if(i!=j){
				       if(arr[i]+arr[j]==k){
				    count++;
				       }
				  }
			       }
	       } 
                 return count;
	} 

      public static void main(String [] args){
      Scanner sc= new Scanner (System.in);
      System.out.println("Enter the size of an array:");
      int n= sc.nextInt();
      int [] arr = new int [n];
      System.out.println("Enter the array elements:");
      for(int i=0; i<n; i++){
	arr[i]=sc.nextInt();
      } 
      System.out.println("Enter the k value:");
      int k= sc.nextInt();
      int ret = pair (arr,k);
      System.out.println("pair: "+ ret);
      }
 }
           
