/* Q1. Max Min of an Array
 Problem Description
- Given an array A of size N.
- You need to find the sum of the Maximum and Minimum elements in the given array.
Problem Constraints
1 <= N <= 105
-109 <= A[i] <= 109
Example Input
Input 1: A = [-2, 1, -4, 5, 3]
Input 2: A = [1, 3, 4, 1]
Example Output
Output 1: 1
Output 2: 5
Example Explanation
Explanation 1:
Maximum Element is 5 and Minimum element is -4. (5 + (-4)) = 1.
Explanation 2:
Maximum Element is 4 and Minimum element is 1. (4 + 1) = 5.
*/

 import java.util.*;
 class MaxMin {
	 static int minmaxadd(int[] arr) {
		 int max=Integer.MIN_VALUE;
		 int min=Integer.MAX_VALUE;
		 for (int i:arr){
			 if(i>max)
				 max=i;
			 if(i<min)
				 min=i;
		 } 
		 return max+min;
	 }
	 public static void main(String [ ] args){
		 Scanner sc = new Scanner(System.in);
		 System.out.println("Enter the size of array:");
		 int n = sc.nextInt();
		 System.out.println("Enter the Elements in array:");
		 int [] arr = new int[n];
		 for(int i=0;i<n;i++){
			 arr[i]=sc.nextInt();
		 }
		 int ret = minmaxadd(arr);
		 System.out.println("the addition of max and min is: "+(ret));
	 }
 }

      

