/* Time to equality
Problem Description
- Given an integer array A of size N.
- In one second, you can increase the value of one element by 1.
- Find the minimum time in seconds to make all elements of the array
equal.
Problem Constraints
1 <= N <= 1000000
1 <= A[i] <= 1000
Example Input
A = [2, 4, 1, 3, 2]
Example Output
8
Example Explanation
We can change the array A = [4, 4, 4, 4, 4]. The time required will be 8
seconds.
*/
   
  import java.util.*;
  class ArrayDemo {
	  static int timetaken(int arr []){
		  int max=Integer.MIN_VALUE;
		  for(int i:arr){
			  if(i>max)
				  max=i;
		  }
		  int itr=0;
		  for(int i:arr){
			  if(i!=max){
				  itr+=(max-i);
			  } 
		  } 
		  return itr;
	  } 
	  public static void main(String [] args){
		  Scanner sc = new Scanner(System.in);
		  System.out.println("Enter the size of Array:");
		  int n= sc.nextInt();
		  int arr[] = new int[n];
		  System.out.println("Enter the elements :");
		  for(int i=0;i<n;i++){
			  arr[i]=sc.nextInt();
		  }
		  int ret = timetaken(arr);
		  System.out.println(" time taken in second is: "+ret);
	  } 
  }
