/* Given an integer Array A of size N and an integer B 
 * You have to return the same array 
 * After rotating it's B times towords the right
 *    Example :1
 *    Input : A:[1,2,3,4] B=2
 *    Output: [3,4,1,2]
 *  ---Example:2 ----
 *    Input : A:[2,5,6] B=1
 *    Output: [6,2,5]
 */
 
  import java.util.*;
  class RotationArray {
         static void revers(int [] arr, int s,int e){ 
		 int temp=0;
		 while(s<e) {
			 temp= arr[s];
			 arr[s]=arr[e];
			 arr[e]=temp;
			 s++;
			 e--;
		 }  
	 } 
       public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);
      System.out.println("Enter the size of an array");
      int N= sc.nextInt();
      int arr[] = new int[N];
     System.out.println("Enter the Elements in array:"); 
     for(int i=0; i<N; i++) {
	arr[i] = sc.nextInt();
     }
      System.out.println("Enter the Rotation Number:");
      int b = sc.nextInt();
      b=b%N;
      revers(arr,0,N-1);
      revers(arr,0 ,b-1);
      revers(arr,b,N-1);
      for(int num:arr) { 
	      System.out.print(num+" ");
      }
       }
  }     


