/* Given a signed 32-bit integer x, return x with its digits reversed. If reversing
x causes the value to go outside the signed 32-bit integer range [-231, 231,-1], then return 0.
Assume the environment does not allow you to store 64-bit integers (signed or unsigned).
Example 1:
Input: x = 123
Output: 321
Example 2:
Input: x = -123
Output: -321
Example 3:
Input: x = 120
Output: 21
Constraints:
-231 <= x <= 231 - 1
*/
import java.util.*;
class ReverseInteger {
	 static int reverse(int x) {
	       int rev=0;
	       int INT_MAX = 2147483647;  // 2^31 - 1
	       boolean n = true;
	       if(x<0){
		       n=false;
		       x=x*-1;
	       }
	       while(x!=0){
		       int digit = x%10;
		       if(rev>(INT_MAX-digit)/10)
			       return 0;
		       rev=rev*10+digit;
		       x=x/10;
	       } 
	       if(n)
		       return rev;
	       return rev*-1;
	 } 
	 public static void main(String [] args) {
                Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Number: ");
		int x = sc.nextInt();
		int ret = reverse(x);
		System.out.println( "Reverse Number is: "+ret);

	 } 
}























