/* iterate throught matrix column by row
  1  2  3  4      1 5 9
  5  6  7  8   => 2 6 10
  9  10 11 12     3 7 11
                  4 8 12
*/
import java.util.*;
class ColumnByRow {
	void columnByRow (int arr[][]){
		for(int i=0;i<arr[0].length;i++){
			for(int j=0;j<arr.length;j++){
				System.out.print(arr[j][i]+" ");
			}
			System.out.println();
		}
	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		int arr[][]=new int[][]{{1,2,3,4},{5,6,7,8},{9,10,11,12}};  
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				System.out.print(arr[i][j]+" ");
			}
			System.out.println();
		} 
		ColumnByRow obj= new ColumnByRow();
		System.out.println("Column by row matrix");
		obj.columnByRow(arr);
	}
}


