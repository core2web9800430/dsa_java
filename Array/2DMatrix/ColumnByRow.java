/* Iterate through entire matrix column by row 
 1 2 3      1 4 7
 4 5 6   => 2 5 8
 7 8 9      3 6 9
Note: it for squre matrix only 
*/

import java.util.*;
class MatrixDemo {
	public static void main(String [] args){
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the number of rows:");
		int row = sc.nextInt();
//		int arr[][] = new int [][]{{1,2,3},{4,5,6},{7,8,9}};
	 	int arr[][] = new int[row][row];
		System.out.println("Enter the Elements of matrix:");
		for(int i=0; i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				arr[i][j]=sc.nextInt();
			}
		} 
		System.out.println("Origenal Matrix");
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				System.out.print(arr[i][j]+" ");
			} 
			System.out.println();
		}
		System.out.println("Column by rows is:");
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				System.out.print(arr[j][i]+" ");
			} 
			System.out.println();
		}
	}
}

