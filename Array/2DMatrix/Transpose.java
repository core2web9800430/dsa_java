/*Given a Squre matrix of size N*N Convert the matrix to its Transpose Matrix 
   
   1  2  3  4        1 5 9  13
   5  6  7  9    =>  2 6 10 14
   9  10 11 12       3 7 11 15
   13 14 15 16       4 9 12 16
*/

import java.util.*;
class Transpose {
	public static void main(String [] args){
		Scanner sc = new Scanner (System.in);
		int arr[][]=new int[][]{{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
		int arr2[][]= new int[arr.length][arr[0].length];
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr.length;j++){
				arr2[j][i]=arr[i][j];
			}
		}
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr.length;j++){
				System.out.print(arr2[j][i]+" ");
			} 
			System.out.println();
		} 
	}
}

