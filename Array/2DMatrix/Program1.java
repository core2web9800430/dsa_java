// Take input from user and print 3X3 matrix 

import java.util.*;
class MatrixDemo {
	public static void main(String [] args){
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the number of rows: ");
		int row = sc.nextInt();
		int arr[][]= new int [row][row];
	        System.out.println("Enter the Elements in array:");
		for( int i=0; i<row; i++){
			for(int j=0;j<row;j++){
				arr[i][j]=sc.nextInt();
			}
		}
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
			System.out.print(arr[i][j]+" ");
		} 
		System.out.println();
		}
	} 
}
			       

