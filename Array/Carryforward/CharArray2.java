/* Given an character array (lowercase) return the  count of pair (i,j) such that
 * a) i<j
 * b) arr[i]='a'
 *    arr[j] ='b'
 *   Arr:[b,a,a,a,c,g,g,g]
 *   Output: 9
 */
import java.util.*;
class ArrayDemo {
	public static void main(String [] args){
		char arr[] = new char[]{'b','a','a','a','c','g','g','g'};
		int count = 0;
		int pair = 0;
		for (int i=0;i<arr.length;i++){
			if(arr[i]=='a'){
				count++;
			} else if(arr[i]=='g'){
				pair+=count;
			}
		} 
		System.out.println("the count of pair is: "+pair);
	}
}
