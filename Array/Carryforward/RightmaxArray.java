/* Given an integer array of size N
   Build an array Rightmax of size N Rightmax of i contains the maximum for 
   the Index i to the index N-1.
    Input : Arr: [-3,6,2,4,5,2,8,-9,3,1]
            N:10.
    Output: [8,8,8,8,8,8,8,3,3,1]	    
*/
import java.util.*;
class RightmaxArray {
	static void rightmaxArray(int [] arr,int n){
		int [] rightmax = new int [n];
		rightmax[n-1]=arr[n-1];
		for(int i=n-2;i>=0;i--) {
			if(arr[i]>rightmax[i+1]){
				rightmax[i]=arr[i];
			} else {
                                rightmax[i]=rightmax[i+1];
			} 
		} 
		for(int i:rightmax) {
			System.out.print(i+" ");
		}
	} 
	 public static void main(String [] args) {
           Scanner sc = new Scanner(System.in);
	   System.out.println("Enter the size of an array:");
            int n=sc.nextInt();
	    int [] arr = new int [n];
	    System.out.println("Enter the Elements in array:");
	    for(int i=0;i<n;i++){
                   arr[i]=sc.nextInt();
	    } 
	    rightmaxArray(arr,n);
	 }
}










