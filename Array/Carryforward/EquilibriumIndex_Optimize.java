/*-you are given an Array A of  integers of size of N
-Your task is to find the equilibrium index of the given array
- The equilibrium index of array is a index such that the sum of elements at lower indexes is equal to the sum of elements at higher indexes.
-if there are no elements that are at lower index or at higher indexes than the corsponding  sum of elements is considered as o.
Examples: 

    Input: A[] = {-7, 1, 5, 2, -4, 3, 0} 
        Output: 3 
	    3 is an equilibrium index, because: 
    Input: A[] = {1, 2, 3} 
       Output: -1
*/
class Equilibrium {
	 int equi(int [] arr){
		 int n=arr.length;
		if(arr[0]==0)
			return 0;
       for(int i=1;i<n;i++){
	       arr[i]=arr[i-1]+arr[i];
       }
	for(int i=1;i<n;i++){
	 int leftsum =arr[i-1];
	 int rightsum = arr[n-1]-arr[i];
		if(leftsum == rightsum){
	 return i;
	 } 
	}	 
  return -1;
 }   
	public static void main(String[] args){
		int arr[] = new int []{-7,1,5,2,-4,3,0};
		Equilibrium obj=new Equilibrium ();
		int ret=obj.equi(arr); 
		System.out.println("Equilibrium at index: "+ ret);                                                                                                                                                                            
	}
}

