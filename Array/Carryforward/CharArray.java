/* Given an character array (lowercase)
   return the count of pair(i j)
   such that 
   a) i<j
   b) arr[i]='a'
      arr[j]='g'

    Input : arr['a','b','e','g','a','g'] 
    Output: 3
*/
  import java.util.*;
  class CharArray {
	 public static void main(String [] args) {
                 Scanner sc=new Scanner(System.in);
		 System.out.println("Enter the size of array");
		 int n= sc.nextInt();
		 System.out.println("Enter the Charecter in  array:");
		 char arr[]=new char[n];
		 for(int i=0; i<n; i++) {
			 arr[i]=sc.next().charAt(0);
		 } 
		 int countA=0;
		 int totalCount=0;
		 for(int i=0; i<n; i++) { 
	               if (arr[i] == 'a') {
		              countA++;
		        } else if (arr[i] == 'g'){
			      totalCount += countA; 
		        }
	         } 
		 System.out.println("count is: "+totalCount); 
	 }  
  }



