/* Given an integer array of size N
   Build an array leftmax of size N leftmax of i contains the maximum for 
   the Index 0 to the index i.
    Input : Arr: [-3,6,2,4,5,2,8,-9,3,1]
            N:10.
    Output: [-3,6,6,6,6,6,8,8,8,8]	    
*/
import java.util.*;
class LeftmaxArray {
	static void leftmaxArray(int [] arr,int n){
		int [] leftmax = new int [n];
		leftmax[0]=arr[0];
		for(int i=1;i<n;i++) {
			if(arr[i]>leftmax[i-1]){
				leftmax[i]=arr[i];
			} else {
                                leftmax[i]=leftmax[i-1];
			} 
		} 
		for(int i:leftmax) {
			System.out.print(i+" ");
		}
	} 
	 public static void main(String [] args) {
           Scanner sc = new Scanner(System.in);
	   System.out.println("Enter the size of an array:");
            int n=sc.nextInt();
	    int [] arr = new int [n];
	    System.out.println("Enter the Elements in array:");
	    for(int i=0;i<n;i++){
                   arr[i]=sc.nextInt();
	    } 
	    leftmaxArray(arr,n);
	 }
}










