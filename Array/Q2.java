 // Given an Integer array of size N count the No of element having atlist 1 element greter than itself.
 //  Input: arr = {2,5,1,4,8,0,8,1,3,8}
 //         N=10.
 //  Output: 7
 
 import java.util.*;
 class ArrayDemo{
	 static int countArray(int [] arr,int n){
		 int count =0;
		 int max= Integer.MIN_VALUE;
		 for(int i=0; i<n; i++){
			 if(arr[i]>max)
				 max=arr[i];

		 }
		 for(int i=0; i<n; i++){
			 if(arr[i]==max)
				 count++;
		 }
		 return n-count;
	 } 
	 public static void main(String [] args){
		 Scanner sc= new Scanner(System.in);
		 System.out.println("Enter the size of an array:");
		 int n= sc.nextInt();
		 int arr[]= new int[n];
		 System.out.println("Enter the array Elements ");
		 for(int i=0; i<n; i++){
			 arr[i]=sc.nextInt();
		 } 
		 int ret = countArray(arr,n);
		 System.out.println("Ans:"+ ret);
	 }
 }
		 

