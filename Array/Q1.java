// Given an array of size n ,
// Print all the elements
  
import java.util.*;
class ArrayDemo {
	static void printArray(int [] arr, int n){
	     for (int i=0;i<n;i++){
		     System.out.print(arr[i]+" ");
	     }
	} 
	public static void main(String [] args){
        Scanner sc = new Scanner(System.in);
	System.out.println("Enter the size of an Array:");
	int n =sc.nextInt();
	System.out.println("Enter the Elements of array:");
	int arr[] = new int [n];
	for(int i=0;i<n;i++){
		arr[i]=sc.nextInt();

	} 

	printArray(arr,n);
	}
}



