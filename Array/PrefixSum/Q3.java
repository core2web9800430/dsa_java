// Prefixsum Approach. 
/* Given an Array of size N amd Q Number of queries                                                    
 * Query contains two parameters [s e] s=> Starting Index , e=> Ending Index 
 * For all queres print the sum of all elements from index 's' to index 'e'. 
 * *Input: arr:[-3,6,2,4,5,2,8,-9,3,1] N=10 ,  Q=3 .                          
            Queries     s    e    sum                                                                                                                                                  Queries1:  1    3     12                                                                                                                                                  Queries2:  2    7     12        
             Queries3:  1    1     6 */    

import java.util.*;
class ArrayDemo { 
	public static void main(String [] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the size of an array:");
		int n = sc.nextInt();
		System.out.println("Enter the Elements in array:");
		int [] arr= new int[n];
		for(int i =0;i<n;i++){
			arr[i] = sc.nextInt();
                       } 
                  System.out.println("Enter the count of queries:");
		  int Q = sc.nextInt();
		  int [] psArr = new int[n];
		   psArr[0] = arr[0];
		   for(int i=1;i<n;i++) {
			   psArr[i]=psArr[i-1] +arr[i];
		   } 
		   int sum = 0;
		   for(int i=0; i<Q; i++) {
			   System.out.println("Enter the Starting Index");
			   int s =sc.nextInt();
			   System.out.println("Enter the Ending Index:");
			   int e = sc.nextInt();
			   if(s==0) {
				   System.out.println("sum is: "+psArr[e]);
			   } else {
				   System.out.println("sum is: "+(psArr[e] - psArr[s-1]));
			   } 
		   } 
	} 
}




        


