/* Given an array A of N integers , constuct prefix sum of the array in the given array itself
 *Return an array of integer denoting the prifix sum of the given array
  Example :
  Input1: A=[1,2,3,4,5]
  Input2: A=[4,3,2]

Output1: [1,3,6,10,15] 
Output2: [4,7,9]
*/
 
import java.util.*;
 class ArrayDemo {
	static void preFixSum(int arr[] ,int n) {  
                   for(int i=1;i<n;i++) {
			   arr[i]=arr[i-1] +arr[i];
		   } 

	}
		 public static void main(String [] args) {
			 Scanner sc = new Scanner(System.in);
			 System.out.println("Enter the size of array:");
                          int n= sc.nextInt();
			  int [] arr= new int[n];
			  System.out.println("Enter the elements in array:");
			  for(int i=0; i<n;i++){
				  arr[i]= sc.nextInt();
			  } 
			  preFixSum(arr,n); 
			  for(int num:arr) {
                            System.out.print(num+" ");
			  }
		 } 
	} 


