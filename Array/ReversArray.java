// take an array revers that array T.C.=> o(N) , S.C. => o(1);

 import java.util.*;
 class ReversArray {
	 static void revers(int[] arr, int n) {
		 int i=0;
		 int j= n-1;
		 int temp=0;
		 while(i<j){
			 temp =arr[j];
			 arr[j]=arr[i];
			 arr[i]= temp;
			 i++;
			 j--;
		 }
	 }
	 public static void main(String [] args){
		 Scanner sc = new Scanner(System.in);
		 System.out.println("Enter the size of array:");
		 int n = sc.nextInt();
		 System.out.println("Enter the array elements:");
		 int [] arr = new int[n];
		 for (int i=0; i<n;i++){
			 arr[i]=sc.nextInt();
		 }
		 revers(arr,n);
		 for(int num :arr){
			 System.out.print(num+" ");
		 }
	 }

 } 
