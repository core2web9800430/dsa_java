/*
Given an array of non-negative integers representing a number, implement a function to
simulate the carry forward operation that occurs when adding 1 to the number represented by
the array. The array represents the digits of the number, where the 0th index is the least
significant digit. Your task is to handle the carry forward operation correctly, updating the array
accordingly. The function should return the resulting array.
For example, given the input array [1, 9, 9], representing the number 199, the function should
return [2, 0, 0], representing the result of adding 1 to 199 with the carry forward properly
handled.
Consider edge cases such as when the number has trailing zeros or when the carry forward
results in an additional digit. Optimize your solution for efficiency and discuss the time and
space complexity of your algorithm.
*/
import java.util.*;
class  ArrayDemo {
	static int [] addOne(int arr[],int n){
		for (int i=n-1;i>=0;i--){
			if(arr[i]<9){
				arr[i]++;
				return arr;
			} else{
				arr[i]=0;
			} 
		} 
		int arr2[] = new int[n+1];
		arr2[0]=1;
		return arr2;
	} 
	public static void main(String [] args){
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the size of an Array:");
		int n=sc.nextInt();
		System.out.println("Enter the Elements in array:");
		int arr[]= new int[n];
		for(int i=0;i<n;i++){
			arr[i]=sc.nextInt();
		}
		arr=addOne(arr,n);
		for(int i:arr){
			System.out.print(i+" ");
		}
	}
}

