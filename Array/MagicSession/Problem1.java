/*-Given an array containing n integers. The problem is to find the sum of the
elements of the contiguous subarray having the smallest(minimum) sum.
Examples:
Input : arr[] = {3, -4, 2, -3, -1, 7, -5}
Output : -6
Subarray is {-4, 2, -3, -1} = -6
Input : arr = {2, 6, 8, 1, 4}
Output : 1
*/
import java.util.*;
class SubArrray {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of an array");
		int n=sc.nextInt();
		System.out.println("Enter the Elements in array:");
			int arr[] = new int[n];
		for(int i=0;i<n;i++){
			arr[i]=sc.nextInt();
		}
		int minsum=arr[0];
		int sum =minsum;
		for(int i=1;i<n;i++){
			if(arr[i]< sum+arr[i]){
				sum=arr[i];
			}else{ 
				sum = sum+arr[i];
			} 
			if(sum<minsum){
				minsum=sum;
			} 
		}

		System.out.println("The smallest sum of subarray is: "+minsum);
	}
}

