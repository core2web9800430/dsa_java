/* Given an array of size N-1 such that it only contains distinct integers in the range of 1 to N. Find
the missing element.
Example 1:
Input:
N = 5
A[] = {1,2,3,5}
Output: 4
Example 2:
Input:
N = 10
A[] = {6,1,2,8,3,4,7,10,5}
Output: 9
*/

import java.util.*;
class ArrayDemo { 
       static int missingElement(int arr[],int n){
	       int mising=0;
	       int totalsum=0;
	       int arraysum=0;
	       for(int i=1;i<=n;i++){
		totalsum+=i;
	       }
	       for(int i:arr){
		       arraysum+=i;
	       }
	       mising = totalsum-arraysum;
	return mising;
       }
	public static void main(String [] args){
	 Scanner sc = new Scanner(System.in);
 	System.out.println("Enter the size of an array:");
	int n= sc.nextInt();
	System.out.println("Enter distinct integers in the range of 1 to N");
	int arr[] =new int[n-1];
	for(int i=0;i<n-1;i++){
	arr[i]=sc.nextInt();
	}
	int ret = missingElement(arr,n);
	System.out.println(" missing element is: "+ret);
	}
}	
