/* Given an array Arr[] that contains N integers (may be positive, negative or zero). Find the
product of the maximum product subarray.
Example 1:
Input:
N = 5
Arr[] = {6, -3, -10, 0, 2}
Output: 180
Explanation: Subarray with maximum product
is [6, -3, -10] which gives the product as 180.
Example 2:
Input:
N = 6
Arr[] = {2, 3, 4, 5, -1, 0}
Output: 120
Explanation: Subarray with maximum product
is [2, 3, 4, 5] which gives the product as 120.
*/

import java.util.*;
class SubArray{
	static int product(int arr[]){
		int totalProduct=1;
		int max=1;
		for(int i:arr){
			totalProduct*=i;
			if(totalProduct>max)
				max=totalProduct;
		}
		return max;
	}
	public static void main(String [] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the Array size:");
		int n=sc.nextInt();
		int arr[] = new int[n];
		System.out.println("Enter the array Elements");
		for(int i=0; i<n; i++){
			arr[i]=sc.nextInt();
		}
		int ret= product(arr);
		System.out.println(" the maximum product of sub array is:"+ret);
	}
}


