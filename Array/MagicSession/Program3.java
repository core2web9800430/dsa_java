/* Given a string s and an array of string words, determine whether s is a prefix string of words.
A string s is a prefix string of words if s can be made by concatenating the first k strings in words
for some positive k no larger than words.length.
Return true if s is a prefix string of words, or false otherwise.
Example 1:
Input: s = "iloveleetcode", words = ["i","love","leetcode","apples"]
Output: true
Explanation:
s can be made by concatenating "i", "love", and "leetcode" together.
Example 2:
Input: s = "iloveleetcode", words = ["apples","i","love","leetcode"]
Output: false
Explanation:
It is impossible to make s using a prefix of arr.
*/
import java.util.*;
class StringDemo {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the string:");
		String str= sc.nextLine() ;
		System.out.print("Enter the numbes of words:");
		int n=sc.nextInt();
		System.out.println("Enter the words:");
		sc.nextLine();
		String [] str2=new String[n];
		for(int i=0;i<n;i++){
		str2[i]= sc.nextLine();
		}
		String sum= "";
		boolean flag=true;
		for(String s:str2){
			sum = sum+s;
			if(sum.equals(str)){
				flag=false;
				System.out.println("True");
			}
		}
		if(flag)
		System.out.println("False");
	}
}

