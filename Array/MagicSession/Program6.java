/* Given an array A of n positive numbers. The task is to find the first equilibrium point in an array.
Equilibrium point in an array is an index (or position) such that the sum of all elements before
that index is the same as the sum of elements after it.
Note: Return equilibrium point in 1-based indexing. Return -1 if no such point exists.
Example 1:
Input:
n = 5
A[] = {1,3,5,2,2}
Output:
3
Explanation:
equilibrium point is at position 3 as sum of elements before it (1+3) = sum of elements after it
(2+2).
Example 2:
Input:
n = 1
A[] = {1}
Output:
1
*/

import java.util.*;
class Equilibrium {
	 static int equilibrium(int arr[],int n){
		for(int i=1;i<arr.length;i++){
			arr[i]=arr[i-1]+arr[i];
		} 
		if(n==1)
			return 1;
		for(int i=1;i<arr.length;i++){
			if( arr[i-1] == arr[n-1]-arr[i])
				return i+1;
		} 
		return -1;
	}
	public static void main(String [] args){
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the size of an Array:");
		int n= sc.nextInt();
		int arr[] =new int[n];
		System.out.println("Enter the Array Elements");
		for(int i=0;i<n;i++){
			arr[i]=sc.nextInt();
		}
		int ret = equilibrium(arr,n);
		System.out.println("Equilibrium point found at position: "+ret);
	}
}

/*
public static int equilibriumPoint(long arr[], int n) {
	int totalsum= 0;
	int currentsum=0;
	for (long i:arr){
		totalsum +=i; 
	}
	for (int i = 0; i < arr.length; i++) {
		totalsum-=arr[i];
		if (totalsum==currentsum)
			return i + 1;
		currentsum+=arr[i];
	}
	return -1;
} 
*/
