/* Given an array of positive integers nums and a positive integer target, return the minimal length
of a
subarray
whose sum is greater than or equal to target. If there is no such subarray, return 0 instead.
Example 1:
Input: target = 7, nums = [2,3,1,2,4,3]
Output: 2
Explanation: The subarray [4,3] has the minimal length under the problem constraint.
Example 2:
Input: target = 4, nums = [1,4,4]
Output: 1
Example 3:
Input: target = 11, nums = [1,1,1,1,1,1,1,1]
Output: 0
*/
import java.util.*;
class ArrayDemo {
	static int subArray(int arr[] ,int target){
		 int len = arr.length;
		int  totalsum=0;
		 for(int i:arr){
			 totalsum+=i;
			 if (i==target)
				 return 1;
		 } 
		 for(int i=0;i<arr.length;i++){
			 totalsum-=arr[i];
			 if(totalsum>=target)
				 len--;
		 }
		 if(len!=arr.length)
			 return len;
		 return 0;
	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int n=sc.nextInt();
		System.out.println("Enter the Elements in array:");
		int arr[] = new int[n];
		for (int i=0;i<n;i++){
			arr[i]= sc.nextInt();
		}
		System.out.println("Enter the target Number:");
		int target=sc.nextInt();
		int ret= subArray(arr,target);
		if(ret==0){
		 System.out.println( ret +" \'Not Present\'");
		} else{
		System.out.println(" smallest length of subarray is: "+ret);
		}
	}
}

