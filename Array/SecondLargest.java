// take an array as input and print Second largest elements

import java.util.*;
class SecondLargest {
	static int sLarg (int[] arr, int n) {
		int larg = Integer.MIN_VALUE;
		int sLarg = Integer.MIN_VALUE;
		for (int i=0; i<n; i++) {
			if(arr[i]>larg ) {
				sLarg = larg;
				larg = arr[i];
			} if( arr[i]> sLarg && arr[i] !=larg) {
				sLarg = arr[i];
			}
		} 
		return sLarg;
	}
	public static void main(String [] args ) {
		Scanner sc = new Scanner( System.in);
		System.out.println("Enter the size of array:");
		int n = sc.nextInt();
		int [] arr = new int [n];
		System.out.println("Enter the Elements of array:");
		for(int i =0; i<n; i++) {
			arr[i] = sc.nextInt();
		}
		int ret = sLarg(arr,n);
		System.out.println(" Second Largest Element is: "+ ret);
	}
}
