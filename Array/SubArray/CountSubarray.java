/* Find out numbers of Subarray in the given array
 * Input: arr:[4,2,10,3,12,-2,15]
 */
import java.util.*;
class CountSubarray {
	public static void main(String [] args){
		Scanner sc= new Scanner (System.in);
		int count =0;
		System.out.println("Enter the Size of an Array:");
		int n= sc.nextInt();
		int arr[]= new int[n];
		System.out.println("Eneter the Elements in Array:");
		for(int i=0; i<n;i++){
			arr[i]=sc.nextInt();
		}
		for(int i=0;i<n;i++){
			for(int j=i;j<n;j++){
				count++;
			}
		}
		System.out.println("the numbers of Subarray present is: "+count);
	}
}

/* optimize approach
   use formula 
    count=n(n+1)/2;
         =7(7+1)/2
	 =56/2
	 =28
*/
