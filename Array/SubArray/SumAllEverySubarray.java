/*Given an array of size N 
 
A: print the Sum of every single Subarray
B: print the Sum of every single  Subarray using " Prefixsum"
C: print the Sum of every single  Subarray with Time Complexity o(n^2) and without using 
   extra space complexity " carry forword"
*/

import java.util.*;
class SubArraysSum {
	void brutforce(int [] arr){
		for(int i=0;i<arr.length;i++){
			for(int j=i;j<arr.length;j++){
				int sum=0;
				for(int k=i;k<=j;k++){
				   sum+=arr[k];
				} 
				System.out.println(sum);
			}
		}  // time complexity is O(n^3) S.c.=> O(1)
	}
	void prefixSum(int [] arr){
		int presum [] = new int[arr.length];
		presum[0]= arr[0];
		for(int i=1;i<arr.length;i++){
			presum[i]=presum[i-1]+arr[i];
		} 
		for(int i=0;i<arr.length;i++){
			for(int j=i;j<arr.length;j++){
				if(i==0){
					System.out.println(presum[j]);
				} else {
					System.out.println(presum[j]-presum[i-1]);
				}
			} 
			System.out.println();
		}  // time complexity is O(n^2) S.C.=> O(n)
	}
	void carryforward(int [] arr){
		for(int i=0;i<arr.length;i++){
			int sum=0;
			for(int j=i;j<arr.length;j++){
				sum+=arr[j];
				System.out.println(sum);
			}
			System.out.println();
		}  // time complexity is O(n^2) S.C. => O(1)
	}
			

	public static void main(String [] args){
		Scanner sc = new Scanner (System.in);
		System.out.println("Eneter the size of Array:");
		int n= sc.nextInt();
		int arr[] = new int[n];
		System.out.println("Enetr the elements in array:");
		for(int i=0;i<n;i++){
			arr[i]= sc.nextInt();
		}
		SubArraysSum obj = new SubArraysSum();
		System.out.println(" Sum of subarray is:");
		//obj.brutforce(arr);
		obj.carryforward(arr);
		//obj.prefixSum(arr);

	}
}
