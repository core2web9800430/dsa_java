/* Given an array of size N find the  total sum  of all subArray sum
Input: arr[1,2,3]
Output: 20;
*/
 
import java.util.*;
class Allsum {
	public static void main(String [] args){
		Scanner sc = new Scanner (System.in);
		System.out.println("Enter the size of array:");
		int n= sc.nextInt();
		int arr[] = new int[n];
		System.out.println("Enter the elements in array:");
		for(int i=0; i<n;i++){
			arr[i]=sc.nextInt();
		}
		int totalsum=0;
		for(int i=0;i<n;i++){
			int sum=0;
			for(int j=i;j<n;j++){
				sum+=arr[j];
				totalsum+=sum;
			}
		}
		System.out.println("total sum is: "+totalsum);
		}
}

