/* Given an unsorted array A of size N that contains only positive integers, find a continuous sub-array 
that adds to a given number S and return the left and right index(1-based indexing) of that subarray.
In case of multiple subarrays, return the subarray indexes which come first on moving from left to right.
Note:- You have to return an ArrayList consisting of two elements left and
right. In case no such subarray exists, return an array consisting of element -1.
Example 1:
Input: N = 5, S = 12, A[] = {1,2,3,7,5}
Output: 2 4
Explanation: The sum of elements from 2nd position to 4th position is 12.
Example 2:
Input: N = 10, S = 15, A[] = {1,2,3,4,5,6,7,8,9,10}
Output: 1 5
Explanation: The sum of elements from 1st position to 5th position is 15.
*/

import java.util.*;
class SubarrayDemo {
	static void index(int arr[],int S){
		int start=0;
		int end=0;
		boolean flag = true;
		for(int i=0;i<arr.length;i++){
			int sum =0;
			for(int j=i;j<arr.length;j++){
				sum=sum+arr[j];
				if(sum==S){
					start=i;
					end = j;
					flag= false;
				}
			} 
			if(!flag)
				break;
		}
		if(end>0)
			System.out.println(++start +" "+ ++end);
		else
			System.out.println("-1");
	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array");
		int n = sc.nextInt();
	        int arr[] = new int[n];
		System.out.println("Enter the Elements in array:");
		for(int i=0;i<n;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter the value of S:");
		int S = sc.nextInt();
		index(arr,S);
	}
}

