/* Largest subarray of 0's and 1's
 Given an array of 0s and 1s. Find the length of the largest subarray with equal number of 0s and 1s.
 Example 1:
Input:N = 4, A[] = {0,1,0,1}
Output: 4
Explanation: The array from index [0...3] contains equal numbers of 0's and 1's.
Thus maximum length of subarray having equal number of 0's and 1's is 4.
Example 2:
Input:N = 5, A[] = {0,0,1,0,0}
Output: 2
*/

import java.util.*;
class SubarrayDemo {
	static int subArraySize(int [] arr){
		int count1 = 0;
		int count0 =0;
		for(int i=0;i<arr.length;i++){
                          if(arr[i]==0){
				  count0++;
			  } else if(arr[i]==1){

				  count1+=count0;
			  }
		} 
			  return count1;
	} 
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of an array:");
		int n=sc.nextInt();
		int arr[] = new int[n];
		System.out.println("Enter the Elements in array");
		for(int i=0;i<n;i++){
			arr[i]= sc.nextInt();
		} 
		int ret = subArraySize(arr);
		System.out.println(ret);
	}
}






