/* Given an array A of n positive numbers. The task is to find the first index in
the array such that the sum of elements before it is equal to the sum of
elements after it.
Note: Array is 1-based indexed.
Example 1:
Input:n = 5, A[] = {1,3,5,2,2}
Output: 3
Explanation: For second test case at position 3 elements before it (1+3) = elements after it (2+2).
Example 2:
Input: n = 1, A[] = {1}
Output: 1
Explanation: Since its the only element hence it is the only point.
*/
 
import java.util.*;
class SubarryDemo {
	static int sumEqual(int arr[]){
		for(int i=1;i<arr.length;i++){
			arr[i]=arr[i-1]+arr[i];
		}
		if(arr.length==1)
			return 1;
		for(int i=1;i<arr.length;i++){
			if(arr[i-1] == arr[arr.length-1]-arr[i])
				return ++i;
		}
		return -1;
	} 
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of an array");
		int n =sc.nextInt();
		int arr[] = new int[n];
		System.out.println("Enter the Elements of an array");
		for(int i=0;i<n;i++){
			arr[i]=sc.nextInt();
		}
		int ret = sumEqual(arr);
		System.out.println(" found at: "+ret);
	}
}

