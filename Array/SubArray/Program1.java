/* given an arra of size N print all the elements in a given subarray from start to end 
 *   int arr:[-2,1,-3,4,-1,2,1,-5,4]
 *   start = 3
 *   end = 7
 */

import java.util.*;
class SubArray {
	public static void main(String [] args){
		Scanner sc = new Scanner (System.in);
		System.out.println("Enter the size of an Array:");
			int n= sc.nextInt();
		int arr[] = new int[n];
		System.out.println("Enter the Elements in array:");
		for (int i=0; i<n;i++) {
			arr[i]= sc.nextInt();
		} 
		System.out.println("Enter the Start index:");
		int start = sc.nextInt();
		System.out.println("Enter the  end index:");
	       int end = sc.nextInt();
       		for(int i = start; i<= end ;i++){
		 System.out.print( arr[i]+" ");
		}
	}
}	
