//Given an array of size N print all the subarray of given array

import java.util.*;
class SubArray {
	void printSubarray(int [] arr){
		for(int i=0;i<arr.length;i++){
			for(int j=i;j<arr.length;j++){
				for(int k=i;k<=j;k++){
					System.out.print(arr[k]+" ");
				} 
				System.out.println();
			}
		}
	}
	public static void main(String [] args){
		Scanner sc = new Scanner (System.in);
		System.out.println("Eneter the size of Array:");
		int n= sc.nextInt();
		int arr[] = new int[n];
		System.out.println("Enetr the elements in array:");
		for(int i=0;i<n;i++){
			arr[i]= sc.nextInt();
		}
		SubArray obj = new SubArray();
		System.out.println(" SubArray is:");
		obj.printSubarray(arr);
	}
}
