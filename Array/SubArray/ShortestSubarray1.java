/*-Given an integer array of size N 
-Return the length of the smallest subarray 
-when both the maximum of the array and the minimum of the array 
  Arr:[1,2,3,3,4,6,4,6,3]
*/
 
 class SubArray {
	 static int subarray(int [] arr, int n){
		 int len=0;
		 int minlen=Integer.MAX_VALUE;
		 int max=Integer.MIN_VALUE;
		 int min=Integer.MAX_VALUE;
		 for(int i:arr){
			 if(i<min)
				 min=i;
			 if(i>max)
				 max=i;
		 } 
		 System.out.println("min"+min);
		 System.out.println("max"+max);
		 for(int i=0; i<n;i++){
		 if(arr[i]==min){
		  for(int j=i+1;j<n;j++){
			 if(arr[j]==max){
				 len= j-i+1;
			     if(minlen>len){
				 minlen=len;
				 }
			 } 
		  } 
		 } else if(arr[i] == max){
		  for(int j=i+1;j<n;j++){
			 if(arr[j]==min){
				 len= j-i+1;
			     if(minlen>len){
				 minlen=len;
				 }
			 } 
		      }  
		  } 
		 } 
		 return minlen;
	 } 
	 public static void main(String [] args){
		 int arr[]= new int[]{1,2,3,1,3,4,6,4,6,3};
		 int n=arr.length;
		 int ret=subarray(arr,n);
		 System.out.println("Shortest length of SubArray is: "+ret);
	 }
 }

