// Maximum sum of subarray  using kadenes algorithum 
import java.util.*;
class Kadenes {
	int maxSum (int [] arr){
		int sum = 0;
		int maxsum=Integer.MIN_VALUE;
		for(int i=0; i<arr.length;i++){
			sum=sum+arr[i];
			if(sum>maxsum)
				maxsum=sum;
			if(sum<0)
				sum=0;
		}
		return maxsum;
	}
	public static void main(String [] args ){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of an array: ");
		int n = sc.nextInt();
	        int [] arr= new int [n];
		System.out.println("Enter the elements in Array:");
		for(int i=0; i<arr.length;i++){
			arr[i]=sc.nextInt();
		} 
		Kadenes obj = new Kadenes();
		int ret= obj.maxSum(arr);
		System.out.println(" the maximum sum of subarray is: "+ret);
	}
}
