// Given an integer array of size N fing the Contigous subarray (containing atleast one num)
// return the subArray which has maxsum of elements
// Input:[-2,1,-3,4,-1,2,1,-5,4]
// Output: [4,-1,2,1]

import java.util.*;
class SubArrayDemo {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		//System.out.println("Enter the size of an Array:");
		//int n= sc.nextInt();
		//int [] arr= new int[n];
		//System.out.println("Enter the Elements in Array:");
		//for(int i=0;i<n;i++){
		//	arr[i]= sc.nextInt();
		//}
		int arr[] =new int[]{-2,1,-3,4,-1,2,1,-5,4}; 
		int sum=0;
		int maxsum= Integer.MIN_VALUE;
		int start=0;
		int end=0;
		int x=0;
		for(int i=0;i<arr.length;i++){
			if(sum==0)
				x=i;
			sum=sum+arr[i];
			if(sum>maxsum){
				start= x;
				end=i;
				maxsum=sum;
			}
			if(sum<0)
			sum=0;
		}
		for(int i=start;i<=end;i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
} 

