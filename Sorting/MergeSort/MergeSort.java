// sort the array using merge sort 
import java.util.*;
class MergeSort {
	void sort(int arr[], int start,int end){
		if(start<end){
			int mid = start+(end-start)/2;
			sort(arr,start,mid);
			sort(arr,mid+1,end);
			merge(arr,start,mid,end);
		}
	}
	void merge(int arr[] ,int start,int mid,int end){
		int n1=mid-start+1;
		int n2=end-mid;
		int arr1[] = new int[n1];
		int arr2[] = new int[n2];
		for(int i=0;i<n1;i++){
			arr1[i]=arr[start+i];
		} 
		for(int i=0;i<n2;i++){
			arr2[i]=arr[mid+1+i];
		}
	    	int i=0,j=0,k=start;
		while(i<arr1.length && j<arr2.length){
			if(arr1[i]<arr2[j]){
				arr[k]=arr1[i];
				i++;
			} else{
				arr[k]=arr2[j];
				j++;
			} k++;
		} 
		while(i<arr1.length){
			arr[k]=arr1[i];
			i++;
			k++;
		}
		while(j<arr2.length){
			arr[k]=arr2[j];
			j++;
			k++;
		}
	} 
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int   n= sc.nextInt();
		int arr[]= new int[n];
		System.out.println("Enter the elements in array:");
		for(int i=0; i<n;i++){
			arr[i]= sc.nextInt();
		}
		int start=0;
		int end = arr.length-1;
		MergeSort obj = new MergeSort();
		obj.sort(arr,start,end);
		for(int i:arr){
			System.out.print(i+" ");
		} 
	}
}

