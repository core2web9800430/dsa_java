// merge two sorted array

import java.util.*;
class MergeArray {
	int[] merge(int arr1[] ,int arr2[]){
		int arr3[] = new int[arr1.length+arr2.length];
		int i=0,j=0,k=0;
		while(i<arr1.length && j<arr2.length){
			if(arr1[i]<arr2[j]){
				arr3[k]=arr1[i];
				i++;
			} else{
				arr3[k]=arr2[j];
				j++;
			} 
			k++;
		}
		while(i<arr1.length){
			arr3[k]=arr1[i];
			k++;
			i++;
		}
		while(j<arr2.length){
			arr3[k]=arr2[j];
			k++;
			j++;
		}
		return arr3;
	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of first array: ");
		int n1 = sc.nextInt();
		int arr1[] = new int[n1];
		System.out.println("Enter the sorted array elements: ");
		for(int i=0;i<n1;i++){
			arr1[i]=sc.nextInt();
		}
		System.out.println("Enter the size of Second array: ");
		int n2 = sc.nextInt();
		int arr2[] = new int[n2];
		System.out.println("Enter the sorted array elements: ");
		for(int i=0;i<n2;i++){
			arr2[i]=sc.nextInt();
		} 
		 MergeArray obj = new  MergeArray();
		 int arr3[]= obj.merge(arr1,arr2);
		 for(int i:arr3){
			 System.out.print(i+" ");
		 }
	}
}


