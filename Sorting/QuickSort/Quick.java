// Quick Sort algorithm

import java.util.*;
class ArrayDemo {
	void quickSort(int arr[],int start, int end){
		if(start < end ){
			int pivot = partition(arr,start,end);
			quickSort(arr,start,pivot-1);
			quickSort(arr,pivot+1,end);
		}
	}
	int partition(int arr[],int start,int end){
		int pivot=arr[end];
		int i= start-1;
		for(int j=start;j<end;j++){
			if(arr[j]<=pivot){
				i++;
				int temp=arr[i];
				arr[i]=arr[j];
				arr[j]=temp;
			}
		}
		int temp=arr[i+1];
		arr[i+1]=arr[end];
		arr[end]=temp;
		return i+1;
	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array");
		int n= sc.nextInt();
		int arr[]= new int[n];
		System.out.println("Enter an Array elements:");
		for(int i=0;i<n;i++){
			arr[i]= sc.nextInt();
		}
		ArrayDemo obj= new ArrayDemo();
		obj.quickSort(arr,0,arr.length-1);
		System.out.println("Sorted Array:");
		for(int i:arr){
			System.out.print(i+" ");
		}
		System.out.println();
	}
}

