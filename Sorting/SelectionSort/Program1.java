// sort array using Selection sort  

import java.util.*;
class SelectionSort {
	void sort(int [] arr,int n){
		for(int i=0;i<n-1;i++){
			 int minIdx =i;
			for(int j=i+1;j<n;j++)
				if(arr[minIdx]>arr[j]){
					minIdx=j;
				}  
			int temp = arr[i];
			arr[i]=arr[minIdx];
			arr[minIdx]=temp;
		}
	} 
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
//		int arr[] = new int[]{7,8,4,3,2,5,1,6};
 		System.out.println("Enter the size of an array:");
		int n = sc.nextInt();
		int arr[] = new int[n];
		System.out.println("Enter the Elements in array:");
		for(int i=0; i<n;i++){
			arr[i]= sc.nextInt();
		}
		 SelectionSort obj = new  SelectionSort();
		  obj.sort(arr,n);

		  for(int i: arr){
			  System.out.print(i+" ");
		  }
	}
}




