// sort array using Selection sort recursive call  

import java.util.*;
class SelectionSort {
	void sort(int [] arr,int n){
		if(n<=0)
			return ;
		sort(arr,n-1);
			 int minIdx =n-1;
			for(int j=n;j<arr.length;j++){
				if(arr[minIdx]>arr[j]){
					minIdx=j;
				}  
			} 
			int temp = arr[n-1];
			arr[n-1]=arr[minIdx];
			arr[minIdx]=temp;
	}

	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
//		int arr[] = new int[]{7,8,4,3,2,5,1,6};
 		System.out.println("Enter the size of an array:");
		int n = sc.nextInt();
		int arr[] = new int[n];
		System.out.println("Enter the Elements in array:");
		for(int i=0; i<n;i++){
			arr[i]= sc.nextInt();
		}
		 SelectionSort obj = new  SelectionSort();
		  obj.sort(arr,n);

		  for(int i: arr){
			  System.out.print(i+" ");
		  }
	}
}




