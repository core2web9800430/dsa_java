/*Problem Statement 1:
Given an Integer N and a list arr. Sort the array using bubble sort algorithm.
Example 1:
Input:N = 5, arr[] = {4, 1, 3, 9, 7}
Output: 1 3 4 7 9
Example 2:
Input:N = 10, arr[] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1}
Output: 1 2 3 4 5 6 7 8 9 10
Expected Time Complexity: O(N^2).
Expected Auxiliary Space: O(1).
Constraints:
1 <= N <= 10^3
1 <= arr[i] <= 10^3
*/

class ArrayDemo {
	void bubbleSort(int arr[],int n){
		int ite = 0;
		for(int i=0;i<n;i++){
			boolean flag=true;
			for(int j=0;j<n-i-1;j++){
				ite++;
				if(arr[j]>arr[j+1]){ 
					flag=false;
					int temp= arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
				}
			} 
			if(flag)
				break;
		}
		System.out.println("Iteration: "+ite);
	}
	public static void main(String [] args){
		ArrayDemo obj= new ArrayDemo();
		int arr1[] = new int []{4,1,3,9,7};
		System.out.println("Given Array: ");
		for(int i:arr1){
			System.out.print(i+" ");
		}
		System.out.println();
		System.out.println("Sorted Array:");
		obj.bubbleSort(arr1,arr1.length);
		for(int i:arr1){
			System.out.print(i+" ");
		}
		System.out.println();
		int arr2[] = new int []{10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
		System.out.println("Given Array: ");
		for(int i:arr2){
			System.out.print(i+" ");
		}
		System.out.println();
		System.out.println("Sorted Array:");
		obj.bubbleSort(arr2,arr2.length);
		for(int i:arr2){
			System.out.print(i+" ");
		}
	}
}






