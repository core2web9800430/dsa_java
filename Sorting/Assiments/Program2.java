/*Given an array of size N containing only 0s, 1s, and 2s; sort the array in ascending order.
Example 1:
Input: N = 5, arr[]= {0 2 1 2 0}
Output: 0 0 1 2 2
Explanation:
0s 1s and 2s are segregated into ascending order.
Example 2:
Input: N = 3, arr[] = {0 1 0}
Output:0 0 1
Explanation:
0s 1s and 2s are segregated into ascending order.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
*/
class ArrayDemo {
	void quickSort(int arr[],int start,int end){
		if(start<end){
			int pivotIdx =partition(arr,start,end);
			quickSort(arr,start,pivotIdx-1);
			quickSort(arr,pivotIdx+1,end);
		}
	}
	int partition(int arr[],int start,int end){
		int i=start-1;
		for(int j=start;j<end;j++){
			if(arr[j]<=arr[end]){
				i++;
				int temp=arr[i];
				arr[i]=arr[j];
				arr[j]=temp;
			}
		}
			int temp=arr[i+1];
			arr[i+1]=arr[end];
			arr[end]=temp;
			return i+1;
	}
	public static void main(String [] args){
		int arr[]=new int[]{0,2,1,2,0};
		for(int i:arr){
			System.out.print(i+" ");
		}
		System.out.println();
		ArrayDemo obj= new ArrayDemo();
		obj.quickSort(arr,0,arr.length-1);
		for(int i:arr){
			System.out.print(i+" ");
		}
	}
}
