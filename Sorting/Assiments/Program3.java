/*Given two sorted arrays arr1[] and arr2[] of sizes n and m in non-decreasing order.
Merge them in sorted order without using any extra space. Modify arr1 so that it
contains the first N elements and modify arr2 so that it contains the last M elements.
Example 1:
Input:
n = 4, arr1[] = [1 3 5 7]
m = 5, arr2[] = [0 2 6 8 9]
Output:
arr1[] = [0 1 2 3]
arr2[] = [5 6 7 8 9]
Explanation:After merging the two non-decreasing arrays, we get,0 1 2 3 5 6 7 8 9.
Example 2:
Input:
n = 2, arr1[] = [10 12]
m = 3, arr2[] = [5 18 20]
Output:	arr1[] = [5 10]
arr2[] = [12 18 20]
Explanation:After merging two sorted arrays we get 5 10 12 18 20.
Expected Time Complexity: O((n+m) log(n+m)) Expected Auxiliary Space: O(1)
*/
import java.util.*;
class ArrayDemo {
	public static void main(String [] args){
		int arr1[]= new int []{1,3,5,7};
		int arr2[]= new int [] {0,2,6,8,9};
		for(int i:arr1){
			System.out.print(i+" ");
		}
		for(int i:arr2){
			System.out.print(i+" ");
		}
		int n=arr1.length-1;
		int m=arr2.length-1;
		int i=0;
		int j=0;
		int k=0;
		while(n>0){
			if(arr1[i]<arr2[j]){
				arr1[k]=arr1[i];
			       i++;	
			}else{
				arr1[k]=arr2[j];
				j++;
			} 
			k++;
			n--;
		} 
		k=0;
		System.out.println("i= "+i+"j= "+j);
		while(m>0){
			if(arr1[i]<arr2[j]){
				arr2[k]=arr1[i];
			       i++;	
			}else{
				arr2[k]=arr2[j];
				j++;
			}
			k++;
			m--;
		} 
		 System.out.println("i= "+i+"j= "+j);
		for(int a:arr1){
			System.out.print(a+" ");
		} 
		System.out.println();
		for(int a:arr2){
			System.out.print(a+" ");
		}
	}
}
