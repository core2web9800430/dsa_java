// Sort array using Bubble sort angorithem

class BubbleSort {
	public static void main(String [] args){
		int arr[] = new int[]{1,2,3,5,6,8,7};
		for(int i:arr){
			System.out.print(i +" ");
		} 
		System.out.println();
		int count = 0;
		for(int i=0;i<arr.length;i++){
				boolean flag = true;
			for(int j=0;j<arr.length-i-1;j++){ 
				count++;
				if(arr[j]>arr[j+1]){ 
				       flag=false;	
				int temp = arr[j];
				arr[j]=arr[j+1];
				arr[j+1]=temp;
				} 
				if(flag)
					break;
			}
		} 
		System.out.println("Sorted");
		for(int i: arr){
			System.out.print(i+" ");
		}
	       System.out.println();	
		System.out.println("count: " +count);
	}
}
