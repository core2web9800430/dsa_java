// Sort array using Bubble sort angorithem

class BubbleSort {
	public static void main(String [] args){
		int arr[] = new int[]{7,8,3,5,2,1,6};
		for(int i:arr){
			System.out.print(i +" ");
		} 
		System.out.println();
		int count = 0 ;
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr.length-i-1;j++){ 
				count++;
				if(arr[j]>arr[j+1]){ 
				int temp = arr[j];
				arr[j]=arr[j+1];
				arr[j+1]= temp;
				} 
			}
		} 
		System.out.println("Sorted");
		for(int i: arr){
			System.out.print(i+" ");
		}
	       System.out.println();	
		System.out.println("count: " +count);
	}
}
