// Sort array using Bubble sort angorithem recurive call

class BubbleSort { 
	int count =0;
	void sort(int arr [] , int n){
		if(n==1)
			return;
		boolean flag=true;
		/*for(int i=0;i<n-1;i++){
			if(arr[i]>arr[i+1]){ 
				flag= false;
				int temp = arr[i];
				arr[i]=arr[i+1];
				arr[i+1]=temp;
			} 
		}
			if(flag)
				return;
				*/
		sort(arr,n-1);
		count++;
		if(arr[n-2]>arr[n-1]){
			flag = false;
			int temp = arr[n-2];
			arr[n-2]=arr[n-1];
			arr[n-1]=temp;
		} 
		if(flag)
			return;
		 sort (arr,n-1);
	}
	public static void main(String [] args){
		int arr[] = new int[]{7,8,3,5,2,1,6};
		for(int i:arr){
			System.out.print(i +" ");
		} 
		System.out.println();
		BubbleSort obj= new BubbleSort();
		System.out.println("Sorted");
		obj.sort(arr,arr.length);
		for(int i: arr){
			System.out.print(i+" ");
		}
	       System.out.println();	
	       System.out.println("count: "+obj.count);
	}
}
