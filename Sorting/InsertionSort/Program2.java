// sort array using Insertion Sort

import java.util.*;
class InsertionSort {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int arr[] = new int[] {6, 4, 2, 1, 5, 9, 7}; 
		InsertionSort obj = new InsertionSort();
		obj.fun(arr, 1);
		for (int i : arr) {
			System.out.print(i + " ");
		}
	}
	void fun(int arr[], int n) {
		if (n == arr.length)
			return;
		int element = arr[n];
		insert(arr, n - 1, element);
		fun(arr, n + 1);
	}
	void insert(int arr[], int j, int element) {
		boolean flag = true;
		if (j >= 0 && arr[j] > element) {
			arr[j + 1] = arr[j];
			flag=false;
			insert(arr, j - 1, element);
		}
		if(flag)
		arr[j + 1] = element;
	}
}

