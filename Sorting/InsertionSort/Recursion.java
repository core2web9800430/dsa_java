// sort array using Insertion sort through recursive call    

import java.util.*;
class InsertionSort {
	void sort(int arr [], int n) {
		if(n==1)
			return;
	            sort(arr,n-1);
		    int element = arr[n-1];
		   insert(arr,n-2,element);
	} 
	void insert(int arr[], int j, int element){
                boolean flag = true;   
		if(j>=0 && arr[j]>element){
			flag=false;
		    arr[j+1]=arr[j];
		insert(arr,j-1,element);
		    } 
	            if(flag)
		    arr[j+1]=element;
	}
	public static void main(String [] args){ 
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of an array:");
		int n = sc.nextInt();
		int arr[]=new int[n];
		System.out.println("Enter the elements in array: ");
		for(int i=0; i<arr.length;i++){
			arr[i]=sc.nextInt();
		} 
		for(int i:arr){
			System.out.print(i+" ");
		}
		System.out.println();
		 InsertionSort obj = new  InsertionSort();
		 obj.sort(arr,arr.length);
		for(int i:arr){
			System.out.print(i+" ");
		}
	}
}
