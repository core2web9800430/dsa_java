// sort array using Insertion Sort

import java.util.*;
class InsertionSort {
	public static void main(String [] args){ 
		Scanner sc = new Scanner(System.in);
	//	int arr[] = new int[]{6,4,2,1,5,9,7} ;
		System.out.println("Enter the size of an array:");
		int n = sc.nextInt();
		int arr[]=new int[n];
		System.out.println("Enter the elements in array: ");
		for(int i=0; i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		for(int i:arr){
			System.out.print(i+" ");
		}
		System.out.println();

		for(int i=1;i<arr.length;i++){
			int j= i-1;
			int element=arr[i];
			while(j>=0 && arr[j]>element){
				arr[j+1]=arr[j];
				j--;
			} 
			arr[j+1]= element;
		}
		for(int i:arr){
			System.out.print(i+" ");
		}
	}
}
