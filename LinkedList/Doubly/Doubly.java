// Doubly Linked List and Functions

import java.util.*;
class DoublyLinkedList {
	Node head=null;
	class Node {
		Node prev=null;
		int data;
		Node next=null;
		Node(int data){
			this.data=data;
		}
	}
	void addFirst(int data){
		Node newNode=new Node(data);
		if(head==null)
			head=newNode;
		else{
			newNode.next=head;
			head.prev=newNode;
			head=newNode;
		}
	} 
	void addLast(int data){
		Node newNode=new Node(data);
		if(head==null)
			head=newNode;
		else{
			Node temp=head;
			while(temp.next!=null)
				temp=temp.next;
			newNode.prev=temp;
			temp.next=newNode;
		}
	}
	void addAtPos(int pos,int data){
		if(pos<=0 || pos>=countNode()+2)
			System.out.println(" Invalid position");
		else if(pos==1)
			addFirst(data);
		else if(pos==countNode()+1)
			addLast(data);	
		else{
			Node newNode=new Node(data);
			Node temp = head;
			while(pos-2!=0){
				temp=temp.next;
				pos--;
			}
			temp.next.prev=newNode;
			newNode.next=temp.next;
			newNode.prev=temp;
			temp.next=newNode;
		}
	} 
	int countNode(){
		int count=0;
		if(head==null)
			return 0;
		else{
			Node temp=head;
			while(temp!=null){
				count++;
				temp=temp.next;
			}
		}
		return count;
	}
	void printDLL(){
		if(head==null)
			System.out.println("Empty Linked list");
		else{
			Node temp = head;
			while(temp!=null){
				System.out.print(temp.data);
				temp= temp.next;
				if(temp!=null)
					System.out.print(" <-> ");
			}
			System.out.println();
		}
	}
	void delFirst(){
		if(head==null)
			System.out.println("Empty Linked List");
		else if(countNode()==1)
			head=null;
		else{
			head=head.next;
			head.prev=null;
		}
	}
	void delLast(){
		if(head==null)
			System.out.println("Empty Linked List");
		else if(countNode()==1)
			head=null;
		else{
			Node temp=head;
			while(temp.next.next!=null)
				temp=temp.next;
			temp.next=null;
		}
	} 
	void delAtPos(int pos){
		if(pos<=0 || pos>=countNode()+1)
			System.out.println(" Invalid Position");
		else if(pos==1)
			delFirst();
		else if(pos==countNode())
			delLast();
		else{
			Node temp=head;
			while(pos-2!=0){
				temp=temp.next;
				pos--;
			}
			temp.next=temp.next.next;
			temp.next.prev=temp;
		}
	}
	void revDLL(){
		if(head==null)
			System.out.println("Linked list is empty");
		else{
			Node temp=head;
			while(temp.next!=null)
				temp=temp.next;
			
			Node temp2=temp;
			while(temp!=null){
				Node ws=temp.next;
				temp.next=temp.prev;
				temp.prev=ws;
				temp=temp.next;
			} 
			head=temp2;
		}
	}

} 
class Client {
	public static void main(String[] args){
		char ch;
		DoublyLinkedList DLL= new  DoublyLinkedList();
		do{
			System.out.println(".... Doubly Linked List.... ");
			System.out.println(" 1-addFirst ");
			System.out.println(" 2-addLast  ");
			System.out.println(" 3-addAtPosition ");
			System.out.println(" 4-delFirst ");
			System.out.println(" 5-delLast ");
			System.out.println(" 6-delAtPosition ");
			System.out.println(" 7-countNode ");
			System.out.println(" 8-reverseDLL ");
			System.out.println(" 9-print Doubly linkedList");
			System.out.println(".......................................");
			
			Scanner sc = new Scanner(System.in);
			int choice = sc.nextInt();
			switch(choice){
				case 1:{	
					       System.out.println("Enter the data");
					       int data = sc.nextInt();
					       DLL.addFirst(data);
				}
				break;
				case 2:{	
					       System.out.println("Enter the data");
					       int data = sc.nextInt();
					       DLL.addLast(data);
				}
				break;
				case 3:{	
					       System.out.println("Enter the data");
					       int data = sc.nextInt();
					       System.out.println("Enter the position");
					       int pos = sc.nextInt();
					       DLL.addAtPos(pos,data);
				}
				break;
				case 4:{	
					       DLL.delFirst();
				}
				break;
				case 5:{	
					       DLL.delLast();
				}
				break;
				case 6:{	
					       System.out.println("Enter the Position");
					       int pos = sc.nextInt();
					       DLL.delAtPos(pos);
				}
				break;
				case 7:{	
					       System.out.println(DLL.countNode());
				}
				break;
				case 8:{	
					       DLL.revDLL();
				}
				break;
				case 9:{	
					       DLL.printDLL();
				}
				break;
				default:{
						System.out.println("Invalid choice ");
				}
				break;
			}
			System.out.println(".....Continue Yes or Not...");
			ch=sc.next().charAt(0);
		}
			while(ch=='y'||ch=='Y');
	}
}


