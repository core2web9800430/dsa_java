/*Count nodes of linked list
Given a singly linked list. The task is to find the length of the linked list, where length is defined
as the number of nodes in the linked list.
Example 1:
Input: LinkedList: 1->2->3->4->5
Output: 5
Explanation: Count of nodes in the linked list is 5, which is its length.
Example 2:
Input: LinkedList: 2->4->6->7->5->1->0
Output: 7
Explanation: Count of nodes in the linked list is 7. Hence, the output is 7.
Expected Time Complexity : O(N)
Expected Auxiliary Space : O(1)
Constraints:
1 <= N <= 10^5
1 <= value <= 10^3*/

import java.util.*;
class LinkedList {
	Node head = null;
	class Node {
		int data;
		Node next=null;
		Node(int data){
			this.data=data;
		}
	}
	void add(int data){
		Node newNode= new Node(data);
		if(head==null)
			head=newNode;
		else{
			Node temp=head;
			while(temp.next!=null)
				temp=temp.next;
			temp.next=newNode;
		}
	}
	int countNode(){
		int count=0;
		Node temp = head;
		while(temp!=null){
			count++;
			temp=temp.next;
		}
		return count;
	}
	void print(){
		if(head==null)
			System.out.println("Linked list is empty");
		else{
			Node temp=head;
			while(temp!=null){
				System.out.print(temp.data);
				temp=temp.next;
				if(temp!=head)
					System.out.print(" -> ");
			}
			System.out.println();
		}
	}
				
}
class Client {
	public static void main(String[] args){
		LinkedList ll = new LinkedList();
		Scanner sc = new Scanner(System.in);
		char ch;
		do{
			System.out.println("Linked List ");
			System.out.println("1-add element");
			System.out.println("2-print element");
			System.out.println("3-countNode");
			System.out.println(".....inter your choice..");

			int choice = sc.nextInt();
			switch(choice){
				case 1:{
					       System.out.println("Enter the data");
					       int data= sc.nextInt();
					       ll.add(data);
				} break;
				case 2:{
					       ll.print();
				} break;
				case 3:{
					      System.out.println(ll.countNode());
				} break;
				default:{
						System.out.println("wrong choice");
				} break;

			} 
			System.out.println("do you want continue ?");
			ch=sc.next().charAt(0);
		
		} while(ch=='y'||ch=='Y');
	}
}
