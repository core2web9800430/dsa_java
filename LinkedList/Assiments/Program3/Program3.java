/*Linked List Insertion
Create a link list of size N according to the given input literals. Each integer input is
accompanied by an indicator which can either be 0 or 1. If it is 0, insert the integer in the
beginning of the link list. If it is 1, insert the integer at the end of the link list.
Hint: When inserting at the end, make sure that you handle NULL explicitly.
Example 1:
Input:
LinkedList: 9->0->5->1->6->1->2->0->5->0
Output: 5 2 9 5 6
Explanation:
Length of Link List = N = 5 9 0 indicated that 9 should be inserted in the beginning. Modified Link List = 9.
5 1 indicated that 5 should be inserted in the end. Modified LinkList = 9,5. 
6 1 indicated that 6 should be inserted in the end. Modified Link List = 9,5,6. 
2 0 indicated that 2 should be inserted in the beginning. Modified Link List = 2,9,5,6. 
5 0 indicated that 5 should be inserted in the beginning. Modified Link List = 5,2,9,5,6. 
Final linked list = 5, 2, 9, 5, 6.
Example 2:
Input:
LinkedList: 5->1->6->1->9->1
Output: 5 6 9
Expected Time Complexity: O(1) for insertAtBeginning() and O(N) for insertAtEnd().
Expected Auxiliary Space: O(1) for both.
Constraints:
1 <= N <= 10^4
*/
import java.util.Scanner;
class LinkedList {
	    Node head = null;
	        class Node {
			int data;
			Node next;
			Node(int data) {
				this.data = data;
				this.next = null;
			}
		}
		void addElement(int data, int indicator) {
			Node newNode = new Node(data);
			if (indicator == 0 || head == null) {
				newNode.next = head;
				head = newNode;
			} else {
				Node temp = head;
				while (temp.next != null) {
					temp = temp.next;
				}
				temp.next = newNode;
			}
		}
		void printList(Node head) {
			if (head == null) {
				System.out.println("Linked list is empty");
			} else {
				Node temp = head;
				while (temp != null) {
					System.out.print(temp.data);
					temp = temp.next;
					if (temp != null) {
						System.out.print(" -> ");
					}
				}
				System.out.println();
			}
		}
    		void processInput() {
			Scanner sc = new Scanner(System.in);
			char ch;
			do {
				System.out.println("1 - Add Element");
				System.out.println("2 - Print Element");
				System.out.println("3 - Exit");
				System.out.println("Enter your choice:");
				int choice = sc.nextInt();
				switch (choice) {
					case 1:
						System.out.println("Enter the data:");
						int data = sc.nextInt();
						System.out.println("Enter the indicator (0 or 1):");
						int indicator = sc.nextInt();
						addElement(data, indicator);
						break;
					case 2:
						printList(head);
						break;
					case 3:
						System.out.println("Exiting program.");
						System.exit(0);
					default:
						System.out.println("Invalid choice. Please try again.");
				}
				System.out.println("Do you want to continue? (y/n):");
				ch = sc.next().charAt(0);
			} while (ch == 'y' || ch == 'Y');
		}
} 
class Client{
	    public static void main(String[] args) {
		    LinkedList ll = new LinkedList();
		    ll.processInput();
	    }
}

