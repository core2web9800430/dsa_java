/*Doubly linked list Insertion at given position
Given a doubly-linked list, a position p, and an integer x. The task is to add a new node with
value x at the position just after pth node in the doubly linked list.
Example 1:
Input:
LinkedList: 2<->4<->5
p = 2, x = 6
Output: 2 4 5 6
Explanation: p = 2, and x = 6. So, 6 is inserted after p, i.e, at position 3 (0-based indexing).
Example 2:
Input:
LinkedList: 1<->2<->3<->4
p = 0, x = 44
Output: 1 44 2 3 4
Explanation: p = 0, and x = 44 . So, 44 is inserted after p, i.e, at position 1 (0-based indexing).
Expected Time Complexity : O(N)
Expected Auxiliary Space : O(1)
Constraints:
1 <= N <= 10^4
0 <= p < N */

import java.util.*;
class LinkedList {
	Node head = null;
	class Node {
		Node prev = null;
		int data;
		Node next = null;
		Node (int data){
			this.data=data;
		}
	}
	void add(int data){
		Node newNode=new Node(data);
		if(head==null)
			head=newNode;
		else{
			Node temp = head;
			while(temp.next!=null)
				temp=temp.next;
			newNode.prev=temp;
			temp.next=newNode;
		}
	}
	int countNode(){
		int count=0;
		Node temp=head;
		while(temp!=null){
			count++;
			temp=temp.next;
		}
		return count;
	}
	void addAtPos(int pos ,int data){
		Node newNode=new Node(data);
		if(pos<=0 ||pos>=countNode()+2)
			System.out.println("invalid position");
		else if(pos==countNode()+1)
			add(data);
		else{
			Node temp=head;
			while(pos+1-2!=0){
				temp=temp.next;
				pos--;
			}
			newNode.prev=temp;
			newNode.next=temp.next;
			temp.next.prev=newNode;
			temp.next=newNode;
		}
	}
	void print(){
		if(head==null)
			System.out.println("Linked List is empty");
		else{
			Node temp = head;
			while(temp!=null){
				System.out.print(temp.data);
				temp=temp.next;
				if(temp!=null)
					System.out.print(" -> ");
			}
			System.out.println();
		}
	}
}
class Client {
	public static void main(String [] args){
		LinkedList ll= new LinkedList();
		ll.add(10);
		ll.add(20);
		ll.add(30);
		ll.add(40);
		ll.print();
		ll.addAtPos(3,60);
		ll.print();
	}
}



