/*Find length of Loop  -
Given a linked list of size N. The task is to complete the function countNodesinLoop() that
checks whether a given Linked List contains a loop or not and if the loop is present then return
the count of nodes in a loop or else return 0. C is the position of the node to which the last node
is connected. If it is 0 then no loop.
Example 1:
Input: N = 10, value[]={25,14,19,33,10,21,39,90,58,45}, C = 4
Output: 7
Explanation: The loop is 45->33. So length of loop is 33->10->21->39->90->58->45 = 7. 
The number 33 is connected to the last node to form the loop because according to the input the 4th node 
from the beginning(1 basedindex) will be connected to the last
node for the loop.
Example 2:
Input:N = 2, value[] = {1,0}, C = 1
Output: 2
Explanation: The length of the loopis 2.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 <= N <= 500
0 <= C <= N-1
*/
import java.util.*;
class LinkedList {
	Node head = null;
	class Node {
		int data;
		Node next=null;
		Node(int data){
			this.data=data;
		}
	}

	void add(int data){
		Node newNode= new Node(data);
		if(head==null)
			head=newNode;
		else{
			Node temp=head;
			while(temp.next!=null)
				temp=temp.next;
			temp.next=newNode;
		}
	}
	int countNode(){
		int count=0;
		Node temp = head;
		while(temp!=null){
			count++;
			temp=temp.next;
		}
		return count;
	}
	void print(){
		if(head==null)
			System.out.println("Linked list is empty");
		else{
			Node temp=head;
			while(temp!=null){
				System.out.print(temp.data);
				temp=temp.next;
				if(temp!=head)
					System.out.print(" -> ");
			}
			System.out.println();
		}
	}
	int countLoop(int pos){
		int count=0;
		int count2=0;
		Node temp = head;
		while(temp!=null){
			count++;
			if(count>=pos)
				count2++;
			temp=temp.next;
		}
		if(count2>0)
		return count2;
		return -1;
	}
				
}
class Client {
	public static void main(String[] args){
		LinkedList ll = new LinkedList();
		Scanner sc = new Scanner(System.in);
		char ch;
		do{
			System.out.println("Linked List ");
			System.out.println("1-add element");
			System.out.println("2-print element");
			System.out.println("3-countNode ");
			System.out.println("4-countLoop ");
			System.out.println("..... Inter your choice...");
			int choice=sc.nextInt();
			switch(choice){
				case 1:{
					       System.out.println("Enter the data");
					       int data = sc.nextInt();
					       ll.add(data);
				} break;
				case 2:{
					       ll.print();
				} break;
				case 3:{
					       System.out.println(ll.countNode());
				} break;
				case 4:{
					       System.out.println("Enter the position");
					       int pos=sc.nextInt();
					       System.out.println(ll.countLoop(pos));
				} break;
				default:{
						System.out.println("wrong input");
				}
			}
			System.out.println(" ...do you want continue ?..");
			ch=sc.next().charAt(0);
		}while(ch=='y'||ch=='Y');
	}
}
