// finding midNode data in  Singly Linked List

import java.util.*;
class LinkedList {
	Node head=null;
	class Node {
		Node next=null;
		int data;
		Node(int data){
			this.data=data;
		}
	}
	void add(int data){
		Node newNode = new Node(data);
		if(head==null)
			head=newNode;
		else{
			Node temp=head;
			while(temp.next!=null)
				temp=temp.next;
			temp.next=newNode;
		}
	}
	void print(){
		if(head==null)
			System.out.println("Linked List is Empty:");
		else{
			Node temp=head;
			while(temp!=null){
				System.out.print(temp.data);
				temp=temp.next;
				if(temp!=null)
					System.out.print(" -> ");
			}
			System.out.println();
		}
	}
	void reversItr(){
		if(head==null)
			System.out.println("Linked List is empty");
		else{
			Node prev=null;
			Node curnt=head;
			Node forw = null;
			while(curnt!=null){
				forw=curnt.next;
				curnt.next=prev;
				prev=curnt;
				curnt=forw;
			}
			head=prev;
		}
	}
	void reversRcr(Node curnt,Node prev){
		if(curnt==null){
			head=prev;
			return;
		}else{
		Node forw=curnt.next;
		curnt.next=prev;
		prev=curnt;
		curnt=forw;
		}
		 reversRcr(curnt,prev);
	}
	int mid1(){
		Node temp=head;
		if(head==null)
			System.out.println("Linked list is empty");
		else{
			int count=0;
			while(count!=countNode()/2){
				count++;
				temp=temp.next;
			}
		}
			return temp.data;
	} 
	int mid2(){
		if(head==null){
			System.out.println("Linked list is empty");
			return 0;
		}
			Node slow=head;
			Node fast=head.next;
			while(fast!=null){
			slow=slow.next;
			fast=fast.next;
			if(fast!=null)
				fast=fast.next;
			}
			return slow.data;
	}
	int countNode(){
		int count=0;
		Node temp=head;
		while(temp!=null){
			count++;
			temp= temp.next;
		}
		return count;
	}

}

class Client {
	public static void main(String [] args ){
		LinkedList ll = new LinkedList();
		Scanner sc = new Scanner(System.in);
		char ch;
		do{
			System.out.println("Single Linked List ");
			System.out.println(" 1- add Elements ");
			System.out.println(" 2- print ");
			System.out.println(" 3- ReversItr");
			System.out.println(" 4- ReversRcr");
			System.out.println(" 5- mid-1");
			System.out.println(" 6- mid-2");
			System.out.println(" 7- countNode");
			System.out.println("..... Enter Your Choice.....");
			int choice= sc.nextInt();
			switch(choice){
				case 1:{
					       System.out.println("Enter the Element");
					       int data =sc.nextInt();
					       ll.add(data);
					       break;
				}
				case 2:{
					       ll.print();
				}break;
				case 3:{
					       ll.reversItr();
				}break;
				case 4:{
					       ll.reversRcr(ll.head,null);
				}break;
				case 5:{
					       System.out.println(ll.mid1());
				}break;
				case 6:{
					       System.out.println(ll.mid2());
				}break;
				case 7:{
					       System.out.println(ll.countNode());
				}break;
				default:{
					System.out.println("Invalid Choice");
					break;
				}
			}
			System.out.println("do you want continue ?");
			ch=sc.next().charAt(0);
		}while(ch=='Y' || ch=='y');
	}
}


