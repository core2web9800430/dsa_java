// Singly Linked List 

import java.util.*;
class LinkedList {
	Node head=null;
	class Node {
		int data;
		Node next=null;
		Node(int data){
			this.data= data;
		}
	}
	void addFirst (int data){
		Node newNode = new Node(data);
		if(head==null)
			head= newNode;
		else{
			newNode.next=head;
			head=newNode;
		}
	}
	void addLast (int data){
		Node newNode=new Node(data);
		if(head==null)
			head=newNode;
		else{
			Node temp=head;
			while(temp.next!=null){
				temp=temp.next;
			}
			temp.next=newNode;
		}
	}
	int countNode (){
		int count =0;
		if(head==null)
			return 0;
		else{
			Node temp=head;
			while(temp!=null){
				count++;
				temp=temp.next;
			}
		}
	      return count;
	} 
	void addAtPos (int pos,int data ){
		Node newNode = new Node(data);
		if(pos<=0 || pos >countNode()+2)
			System.out.println(" wrong Input");
		else if(pos==1){
			addFirst(data);
		} 
		else{
			Node temp = head;
			while( pos-2!=0){
			     temp= temp.next;
			     pos--;
			} 
			newNode.next=temp.next;
			temp.next=newNode;
		}
	}
	void delFirst(){
		if(head==null)
			System.out.println("Empty Linked List");
		else if(countNode()==1)
				head=null;
		else
			head=head.next;
	} 
	void delLast(){
		if(head==null)
		        System.out.println("Empty Linked List");
		else if(countNode()==1)
			head=null;
		else {
			Node temp=head;
		 	while(temp.next.next!=null){
				temp=temp.next;
			} 
			temp.next=null;
		}
	} 
	void delAtPos(int pos ){
		if(pos<=0 || pos>=countNode()+2)
			System.out.println("Wrong Input");
		else if(pos ==1)
			delFirst();
		else{
			Node temp = head;
			while(pos-2!=0){
				temp=temp.next;
				pos--;
			} 
			temp.next=temp.next.next;
		}
	}
	void printSLL(){
		Node temp=head;
		if(head==null)
			System.out.println("empty ");
		else{
			while(temp!=null){
				System.out.print(temp.data);
				temp= temp.next;
				if(temp!=null)
					System.out.print("->");
			} 
			System.out.println();
		}
	} 
	void replaceAtPos(int pos,int data){
		addAtPos(pos,data);
		delAtPos(pos+1);
	} 
}

class Client {
	public static void main(String [] args){
		char ch;
		LinkedList SLL = new LinkedList();
		do{
			System.out.println("Singly LinkedList");
			System.out.println(" 1 - addFrist");
			System.out.println(" 2 - addlast ");
			System.out.println(" 3 - addAtPos ");
			System.out.println(" 4 - delFirst ");
			System.out.println(" 5 - delLast ");
			System.out.println(" 6 - delAtPos ");
			System.out.println(" 7 - countNode ");
			System.out.println(" 8 - replaceAtPos");
			System.out.println(" 9 - printSLL");
			System.out.println(".........................");

			Scanner sc = new Scanner(System.in);
			int choice=sc.nextInt();

			switch(choice){
				case 1:{
					       System.out.println("Enter data");
					       int data = sc.nextInt();
					       SLL.addFirst(data);
					} 
					break;
				case 2:{
					       System.out.println("Enter data");
					       int data = sc.nextInt();
					       SLL.addLast(data);
					} 
				        break;
				case 3:{
					       System.out.println("Enter data");
					       int data = sc.nextInt();
					       System.out.println("Enter Position");
					      	int pos= sc.nextInt(); 
					       SLL.addAtPos(pos,data);
					} 
				        break;
				case 4:{
					       SLL.delFirst();
					} 
				        break;
				case 5:{
					       SLL.delLast();
					} 
				        break;
				case 6:{
					       System.out.println("Enter Position");
					       int pos = sc.nextInt();
					       SLL.delAtPos(pos);
					} 
				        break;
				case 7:{
					       int count =SLLi.countNode();
					       System.out.println(count); 
					} 
				        break;
				case 8:{
					       System.out.println("Enter data");
					       int data = sc.nextInt();
					       System.out.println("Enter Position");
					      	int pos= sc.nextInt(); 
					       SLL.replaceAtPos(pos,data);
					} 
				       break;
				 case 9:{
						SLL.printSLL();
				        }
					break;
				default:{
						System.out.println("wrong choice ");
					        break;
				} 
			}
			System.out.println(".........Continue Your choise \"Yes\" or \"No\""); 	
		 ch = sc.next().charAt(0);

		}

			while(ch=='Y' ||ch=='y');
		}
	}

	






