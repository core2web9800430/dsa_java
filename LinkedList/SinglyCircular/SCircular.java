// Singly Circular Linked List and Functions

import java.util.*;
class SinglyCircular {
	Node head=null;
	Node tail=null;
	class Node {
		int data;
		Node next = null;
		Node(int data){
			this.data=data;
		}
	}
	void addFirst(int data){
		Node newNode=new Node(data);
		if(head==null){ 
			head=tail=newNode;
			newNode.next=head;
		}

		else{
			newNode.next=head;
			head=newNode;
			tail.next=head;
		}
	} 
	void addLast(int data){
		Node newNode=new Node(data);
		if(head==null)
			addFirst(data);
		else{
			tail.next=newNode;
			tail=newNode;
			tail.next=head;
		}
	}
	void addAtPos(int pos,int data){
		if(pos<=0 || pos>=countNode()+2)
			System.out.println(" Invalid position");
		else if(pos==1)
			addFirst(data);
		else if(pos==countNode()+1)
			addLast(data);	
		else{
			Node newNode=new Node(data);
			Node temp = head;
			while(pos-2!=0){
				temp=temp.next;
				pos--;
			}
			newNode.next=temp.next;
			temp.next=newNode;
		}
	}
	void delFirst(){
		if(head==null)
			System.out.println("empty linked list");
		else if (countNode()==1){
			head=tail=null;
		}else {
			head=head.next;
			tail.next=head;
		} 
	}
	void delLast(){
		if(head==null)
			System.out.println("empty linked list");
		else if(countNode()==1)
			head=null;
		else{
			Node temp= head;
			while(temp.next.next!=head){
				temp=temp.next;
			}
			temp.next=head;
			tail=temp;
		}
	}
	void delAtPos(int pos){
		if(pos<=0 || pos>=countNode()+1)
			System.out.println("Invalid Position");
		else if(pos==1)
			delFirst();
		else if(pos==countNode())
			delLast();
		else{
			Node temp=head;
			while(pos-2!=0){
				temp=temp.next;
				pos--;
			}
			temp.next=temp.next.next;
		}
	}
	int countNode(){
		int  count=0;
			if(head==null)
				return count;
		        else {
				Node temp=head;
			        while(temp.next!=head){
				count++;
				temp=temp.next;
		         	}
				count++;
			     return count;
			}
	}
	void printSCLL(){
		if(head==null)
			System.out.println("empty Linked list");
		else{
			Node temp = head ;
			while(temp.next!=head){
				System.out.print(temp.data+" ");
				temp=temp.next;
			}
		        System.out.print(temp.data+" ");	
	         	System.out.println();
		}
	}
}
class Client {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		char ch;
		SinglyCircular SCLL=new SinglyCircular();
		do{
			System.out.println(".... Singly Circular Linked List.... ");
			System.out.println(" 1-addFirst ");
			System.out.println(" 2-addLast  ");
			System.out.println(" 3-addAtPosition ");
			System.out.println(" 4-delFirst ");
			System.out.println(" 5-delLast ");
			System.out.println(" 6-delAtPosition ");
			System.out.println(" 7-countNode ");
			//System.out.println(" 8-reverseDLL ");
			System.out.println(" 8-print Singly Circular linkedList");
			System.out.println(".......................................");
			
			int choice = sc.nextInt();
			switch(choice){
				case 1:{	
					       System.out.println("Enter the data");
					       int data = sc.nextInt();
					       SCLL.addFirst(data);
				}
				break;
				case 2:{	
					       System.out.println("Enter the data");
					       int data = sc.nextInt();
					       SCLL.addLast(data);
				}
				break;
				case 3:{	
					       System.out.println("Enter the data");
					       int data = sc.nextInt();
					       System.out.println("Enter the position");
					       int pos = sc.nextInt();
					       SCLL.addAtPos(pos,data);
				}
				break;
				case 4:{	
					       SCLL.delFirst();
				}
				break;
				case 5:{	
					       SCLL.delLast();
				}
				break;
				case 6:{	
					       System.out.println("Enter the Position");
					       int pos = sc.nextInt();
					       SCLL.delAtPos(pos);
				}
				break;
				case 7:{	
					       System.out.println(SCLL.countNode());
				}
				break;
				/*case 8:{	
					       DLL.revDLL();
				}
				break;*/
				case 8:{	
					       SCLL.printSCLL();
				}
				break;
				default:{
						System.out.println("Invalid choice ");
				}
				break;
			}
			System.out.println(".....Continue Yes or Not...");
			ch=sc.next().charAt(0);
		}while(ch=='y'||ch=='Y');
	}
}


		

			






































































































































































































































