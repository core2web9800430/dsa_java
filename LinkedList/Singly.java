// Single linked List 

class Node {
	int data;
	Node next = null;
	Node(int data){
		this.data=data;
	}
} 
class LinkedList {
	Node head = null;
	void addFirst(int data){
		Node newNode=new Node(data);
		if(head == null){
			head=newNode;
		} else{
			newNode.next=head;
			head=newNode;
		}
	} 
	void addLast(int data){
		Node newNode= new Node(data);
		Node temp = head ;
		if(head==null){
			System.out.println("Linked List is Empety");
		} else{
			while(temp.next!=null){
				temp=temp.next;
			}
			temp.next = newNode;
		} 
	}	
	void printSLL(){
		Node temp = head;
		if(temp == null){
			System.out.println(" Linked List is Empety");
		} else {
			while(temp!=null){
				System.out.print(temp.data);
				temp=temp.next;
				if(temp!=null){
					System.out.print(" -> ");
				}
			} 
			System.out.println();
		}
	}
	int countNode(){
		Node temp=head;
		int count=0;
		if(head==null)
			System.out.println("empty");
		else {
			while(temp!= null){
				count++;
				temp=temp.next;
			}
		}
		return count;
	}
}
class Client {
	public static void main(String [] args){
		LinkedList SLL=new LinkedList();
		SLL.addFirst(10);
		SLL.addFirst(20);
		SLL.addFirst(30);
		SLL.addLast(05);
		SLL.printSLL();
		System.out.println(" adding last element 4");
		SLL.addLast(4);
		SLL.printSLL();
		System.out.println(" adding first element 40");
		SLL.addFirst(40);
		SLL.printSLL();

		System.out.println(".....................");
		SLL.printSLL();

	}
}




