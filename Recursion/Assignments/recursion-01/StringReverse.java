// WAP to print string in reverse order.

import java.util.*;
class RevString {
	String reverse(String str){
		if(str==null || str.length() ==1)
			return str;
		return reverse(str.substring(1))+str.charAt(0);
	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the String :");
		String str = sc.nextLine();
		RevString obj = new RevString();
		String str2 = obj.reverse(str);
		System.out.println(str2);
	}
}

