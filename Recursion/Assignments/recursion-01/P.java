//write a proram to print the sum of n natural numbers.

import java.util.*;
class Sum0toN {
	static int sumN(int num){
		int sum =0;
		if(num==0){
			return 0;
		} 
		sumN(--num);
		sum=sum+num;
		return sum;

	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the Number");
		int num = sc.nextInt();
		int ret = sumN(num);
		System.out.println("the sum is: "+ret);
	}
}


					
