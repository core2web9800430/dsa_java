//write a proram to print the sum of n natural numbers.

import java.util.*;
class Sum0toN {
	static int sum=0;
	static int sumN(int num){
		if(num==0)
		       return sum;
		   sum=num+sumN(--num);
		     return sum;
	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the Number");
		int num = sc.nextInt();
		int ret = sumN(num);
		System.out.println("the sum is: "+ret);
	}
}

/* or 
static int sumN(int num){
	if(num==0)
	return 0;
	return num+sum(num-1);
}
*/
					
