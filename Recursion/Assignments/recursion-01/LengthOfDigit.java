//WAP to print the length of digits in a number.

import java.util.*;
class LenDigit {
      int lenDigit(int num){
	      if(num==0)
		      return 0;
	       num = 1+lenDigit(num/10);
	       return num;

      }
      public static void main(String [] args){
	      Scanner sc = new Scanner(System.in);
	      System.out.println("Enter the number:");
	      int num = sc.nextInt();
	      LenDigit obj = new LenDigit();
	      int ret = obj.lenDigit(num);
	      System.out.println(ret);
      }
}
