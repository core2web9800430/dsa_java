// WAP to find the factorial of a number.


import java.util.*;
class Factorial {
	int factorialOf(int num){
		if(num==1)
			return 1;
		num= num*factorialOf(--num);
		return num;
	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number:");
		int num = sc.nextInt();
		Factorial obj = new Factorial();
		int ret = obj.factorialOf(num);
		System.out.println(ret);
	}
}
