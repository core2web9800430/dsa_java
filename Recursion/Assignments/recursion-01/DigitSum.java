// write a program to calculate the sum of digits of a given positive integer.
import java.util.*;
class DigitSum {
	int sumOfDigit(int num){
		if(num==0)
			return 0;
		num=num%10+sumOfDigit(num/10);
		return num;
	}
	public static void main(String [] args){
		Scanner sc = new Scanner (System.in);
		System.out.println("Enter the Number");
		int num = sc.nextInt();
		DigitSum obj = new DigitSum();
		int ret = obj.sumOfDigit(num);
		System.out.println(ret);
	}
}

