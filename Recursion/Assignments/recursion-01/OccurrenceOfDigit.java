// WAP to count the occurrence of a specific digit in a given number.


import java.util.*;
class OccurrenceOfDigit {
	int count=0;
	int digitOccurrence(int num,int digit){
		if(num ==0)
			return 0;
		count = digitOccurrence(num/10,digit);
		if(num%10 == digit)
			return ++count;
		return count;
	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number:");
		int num = sc.nextInt();
		System.out.println("Enter the Digit to be Search in number:");
		int digit= sc.nextInt();
		OccurrenceOfDigit obj = new OccurrenceOfDigit();
		int ret = obj.digitOccurrence(num,digit);
		System.out.println(ret);
	}
}


