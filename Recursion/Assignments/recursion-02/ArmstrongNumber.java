//Write a program to check if a given number is an Armstrong number or not.
//( An Armstrong number is a number that is equal to the sum of its own digits each
  //raised to the power of the number of digits.)

import java.util.*;
class ArmstrongNumber {
	boolean isArmstrongNumber(int num){
		 int count = String.valueOf(num).length();
	        System.out.println("num"+armstrong(num,count));
		return num==armstrong(num,count);
	}
	int armstrong(int num,int count){
		if(num<=1)
			return num;
		armstrong(num/10,count);
		return pow(num%10,count)+armstrong(num/10,count);
	}
	int pow(int num ,int count){
		if(count<=0)
			return 1;
		return num*pow(num,count-1);
	} 
	public static void main(String [] args){
	        Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number");
		int num = sc.nextInt();
		ArmstrongNumber obj = new ArmstrongNumber();
		boolean check = obj.isArmstrongNumber(num);
		if(check)
			System.out.println("It is Armstrong Number");
		else
			System.out.println("Not an Armstrong Number");
	}
}


