// Write a program to print the product of digits of a given number.

import java.util.*;
class ProductOfDigits {
	int productOfDigits(int num){
		if(num==0)
			return 1;
		return num%10*productOfDigits(num/10);
	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number:");
		int num = sc.nextInt();
		ProductOfDigits obj = new ProductOfDigits();
		int ret = obj.productOfDigits(num);
		System.out.println(ret);
	}
}

