// Write a program to print the sum of odd numbers up to a given number.

import java.util.*;
class OddSum {
	int oddSumN(int num){
		if(num==0)
			return 0;
		if(num%2!=0){
			return num+oddSumN(num-1);
		}
			return oddSumN(num-1);
	} 
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Number:");
		int num = sc.nextInt();
		OddSum obj = new OddSum();
		int ret = obj.oddSumN(num);
		System.out.println("Sum of odd number till "+ num+" is: "+ret);
	}
}

