/* Write a program to determine whether a given positive integer is a Deficient
   Number or not. A Deficient Number is a positive integer where the sum of its
   proper divisors is less than the number itself.
*/

  import java.util.*;
  class DeficientNumber {
	  boolean isDeficient ( int num){
		  int sum = sumOfDivisor(num ,1,0);
			  if(sum<num)
				  return true;
		  return false;
	  }
	  int sumOfDivisor(int num, int i, int sum){
		  if(i==num)
			  return sum;
		  if(num%i==0)
			  sum+=i;
		  return sumOfDivisor(num,i+1,sum);
	  }
	  public static void main(String [] args ){
		  Scanner sc = new Scanner(System.in);
		  System.out.println("Enter the Number:");
		  int num = sc.nextInt();
		  DeficientNumber obj = new  DeficientNumber();
		  if(obj.isDeficient(num))
			 System.out.println(num+" is a Deficient Number");
			 
		  else 
			  System.out.println(num+" is Not Deficient Number");
		  System.out.println();
	  }
  }
