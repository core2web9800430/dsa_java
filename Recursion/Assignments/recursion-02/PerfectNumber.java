/* Write a program to check whether a given positive integer is a Perfect Number or not.
(A Perfect Number is a positive integer that is equal to the sum of its proper
 divisors, excluding itself.) 
*/

import java.util.*;
class PerfectNumber {
	/* boolean isPerfectNumber(int num){ 
		int sum=0;
		for(int i=1;i<num;i++){
			if(num%i==0)
				sum+=i;
		} 

		if(sum==num)
			return true;
		return false;

	}  
	//or
          int ser=1;
          int sum=0;
	  int perf(int num){
		  if(ser==num)
			  return 0;

		  if(num%ser==0)
			sum=sum+ser;
		  ser+=1;
		  perf(num); 
		 return sum; 
               } 
	      */
       boolean isPerfectNumber(int num){
			int sum= perfectnum(num,1);
			 return sum==num;
       } 
	int perfectnum(int num ,int sum){
	             if(num==sum)
			     return 0;
		     if(num%sum==0)
			     return sum+perfectnum(num,sum+1);
		     return perfectnum(num,sum+1);
	}

 	  
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Number: ");
		int num = sc.nextInt();
		PerfectNumber obj = new PerfectNumber();
		boolean check = obj.isPerfectNumber(num);
		if(check)
			System.out.println(num+" is Perfect Number");
                 	else
		       	System.out.println(num+" Not a Perfect Number");
	} 
}
	
