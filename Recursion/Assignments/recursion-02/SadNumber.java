/* Write a program to determine whether a given positive integer is a sad number or not.
 An unhappy number is a number that is not happy, i.e., a number n such that iterating 
 this sum-of-squared-digits map starting with n never reaches the number 1.
 The first few unhappy numbers are 2, 3, 4, 5, 6, 8, 9, 11, 12, 14, 15,
*/
import java.util.*;
class SadNumber {
	boolean isSadNumber(int num){
                  String sum= String. valueOf(sadnum(num ,0));
		  while(sum.length()==1){
		  sum+= String.valueOf( sadnum(sadnum(Integer.parseInt(sum) ,0),0) );
		  } 
		  if(sum.charAt(0)=='1')
			  return false;
		  return true;
	} 
	int sadnum(int num ,int sum){
		if(num==0)
			return sum;
		sum=sum+(num%10*num%10);
		return sadnum(num/10,sum);
	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Number:");
		int num=sc.nextInt();
		SadNumber obj= new SadNumber();
		System.out.print("Sad Number till "+num +" is: ");
		for(int i=1; i<= num;i++){
		if(obj.isSadNumber(i))
			System.out.print(i+" ");
	} 
	System.out.println();
    }  
}


		
