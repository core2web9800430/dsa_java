/* Write a program to calculate and print the sum of the first N triangular numbers. Take N as input from the user.
  Triangular Number is termed as triangular number if we can represent it in the form of triangular grid 
  of points such that the points form an equilateral triangle and each row contains as many points as the row number,
  i.e., the first row has one point, second row has two points, third row has three points and so on. 
  The starting triangular numbers are 1, 3 (1+2), 6 (1+2+3), 10 (1+2+3+4).
 */

 import java.util.*;
 class TriangularNumber {
	/*boolean isTriangularNumber(int num){
	       int sum = 0;
	       if(num==1)
		       return true;
	       for(int i=1;i<=num;i++){
		sum+=i;
		if(sum==num){
			   return true;
	       } 
	       } 
	return false;
	} */


	  boolean isTriangularNumber(int num){
		  if(num==1){
			  return true;
		  } else{
			   boolean check = triangularnum(num,0,0);
			//   System.out.println(num+" "+check);
			  if(check)
				  return true;
			  else
				  return false;
		  }
	  } 
	  boolean triangularnum(int num,int sum,int i){
		  if(i==num || sum==num){
			if(sum==num)	  
			      return true;
			  return false;

		  } 
		  sum+=i;
		  return triangularnum(num ,sum, i+1);
	  }	  
	public static void main(String [] args){
	Scanner sc = new Scanner(System.in);
	System.out.println("Enter the Number:");
	int num = sc.nextInt();
	TriangularNumber obj = new TriangularNumber();
	for(int i=1;i<=num;i++){
	   if(obj.isTriangularNumber(i)) 
			System.out.print(i+" ");
	} 
     }
 }	
