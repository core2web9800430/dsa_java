/* Write a program to check if a given number is a Magic Number or not. 
A  Magic Number is a number in which the eventual sum of the digits is equal to 1.
For example
Number= 50113
=> 5+0+1+1+3=10
=> 1+0=1
This is a Magic Number 
*/
 import java.util.*;
 class MagicNumber {
	 boolean isMagicNumber(int num){
		if(digitSum(digitSum(num))==1)
			return true;
		return false;
	 }
	 int digitSum( int num){
		 if(num==0)
			 return 0;
		 return num%10+digitSum(num/10);
	 } 
	public static void main(String [] args){
	       Scanner sc = new Scanner(System.in);
	       System.out.println("Enter the Number");
	       int num = sc.nextInt();
	       MagicNumber obj = new MagicNumber();
	       boolean check = obj.isMagicNumber(num);
	       if(check)
		       System.out.println("Magic Number");
	       else
		       System.out.println(" Not Magic Number");
	}
 }




