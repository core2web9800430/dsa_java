/* Write a Java program to determine whether a given positive integer is an
   automorphic number or not.
   (An automorphic number (also known as a circular number) is a number whose
   square ends with the same digits as the number itself)
*/
import java.util.*;
class AutomorphicNumber {
	boolean isAutomorphic(int num) {
		if(automorphic(num,sqare(num)))
			return true;
		return false;
	}
	int sqare(int num){
		return num*num;
	}
	boolean automorphic(int num,int sqare){
		if(num<=0)
			return true;
		if(num%10 == sqare%10) { 
			System.out.println("num: "+num%10+"  "+"sqre: "+sqare%10);
		return automorphic(num/10,sqare/10); 
		}
		return false;
	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Number");
		int num = sc.nextInt();
		AutomorphicNumber obj = new AutomorphicNumber( );
		if(obj.isAutomorphic(num))
			System.out.println(num+" Is Automorphic Number ");
		else 
			System.out.println(num+" Not Automorphic Number ");
	}
}
	


