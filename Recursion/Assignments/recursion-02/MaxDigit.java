// Write a program to print the maximum digit in a given number.

import java.util.*;
class MaxDigit {
	int maxDigit(int num){
		if(num==0)
			return 0;
		if(num%10> maxDigit(num/10))
			return num%10;

		return maxDigit(num/10);
	} 
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number:");
		int num = sc.nextInt();
		MaxDigit obj = new MaxDigit();
		int ret = obj.maxDigit(num);
		System.out.println(ret);
	}
}


