//Write a program to print the factorial of a given number.

import java.util.*;
class Factorial {
	int factOf(int num){
		if(num==0)
			return 1;
		return factOf(num-1)*num;
	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Number");
		int num= sc.nextInt();
		Factorial obj = new Factorial();
		int ret = obj.factOf(num);
		System.out.println(ret);
	}
}
