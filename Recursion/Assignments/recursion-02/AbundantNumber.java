/* Write a program to check if a given number is an Abundant Number or not.
  (An Abundant number is the sum of all its proper divisors, denoted by sum(n), is
  greater than the number's value.)
  (since 12 is divisible by these numbers, and they are less than 12). 
  The sum of these proper divisors is:
  σ(12) = 1 + 2 + 3 + 4 + 6 = 16
*/ 
  
import java.util.*;
class AbundantNumber { 	
	int  isAbundantNumber(int num ,int i) {
		if(i==num)
			return 0;
		int sum1=0;
		if(num%i==0)
			sum1+=i;
		return sum1+=isAbundantNumber(num,i+1);
	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println(" Enter the Number ");
		int num = sc.nextInt();
		AbundantNumber obj = new AbundantNumber();
		int sum =obj.isAbundantNumber(num,1);                                                                                               
		if(sum>num)
			System.out.println(num +" is Abundant Number");
		else
			System.out.println(num +" Not Anudant number");
	}
}

