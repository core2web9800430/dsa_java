// Write a program to determine whether a given positive integer is a composite number or not.
//A composite number is a natural number or a positive integer which has more than two factors.
//For example, 15 has factors 1, 3, 5 and 15.

import java.util.*;
class CompositeNumber {
	boolean isCompositeNumber(int num){
	         int count = factorsCount(num,1);
		 System.out.println(count);
		if(count>2)
			return true;
		return false;
	}
	int factorsCount(int num,int count){
		if(num==count)
			return 1;
		if(num%count==0)
			return 1+factorsCount(num,count+1);
		return factorsCount(num,count+1);
	} 
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Number:");
		int num = sc.nextInt();
		CompositeNumber obj = new CompositeNumber();
		if(obj.isCompositeNumber(num))
			System.out.println(num+ " Composite Number");
		else 
			System.out.println("Not Composite Number");
	}
}


