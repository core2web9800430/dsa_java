/* Write a program that determines whether a given number is a cube number or not.
  ( A cube number is defined as a number that is the cube of an integer. ex The first four cube numbers are 1, 8, 27, 64)
*/

import java.util.*;
class CubeNumber {
	boolean isCubeNumber(int num){
		if(cube(num,0)||num==1)
			return true;
		return false;
	}
	boolean cube(int num,int i){
		if(i==num)
			return false;
		if(i*i*i==num)
			return true;
		return cube(num,i+1);
	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Number:");
		int num = sc.nextInt();
		CubeNumber obj = new CubeNumber();
		if(obj.isCubeNumber(num))
			System.out.println(num+" Is a Cube Number");
		else
			System.out.println(num+" Not a Cube Number");
	}
}

	
