// Write a program to determine whether a given number is a happy number or not.
// (A happy number is a number which eventually reaches 1 when replaced by the
// sum of the square of each digit.)
// example: 13-> 1^2 + 3^2 = 10. 10-> 1^2 + 0^2 = 1

import java.util.*;
class HappyNumber {
	boolean isHappyNumber(int num){
		int ret = happynum(happynum(num));
		return 1==ret;
	}
	int happynum( int num){
		if(num<=0)
			return num;
		return num%10*num%10+happynum(num/10);
	} 
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Number:");
		int num = sc.nextInt();
		HappyNumber obj = new HappyNumber();
		boolean check = obj.isHappyNumber(num);
		if(check)
			System.out.println("Happy Number");
		else 
			System.out.println("Not Happy Number");
	}
}


