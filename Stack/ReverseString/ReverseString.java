// Reverse String using Stack

import java.util.*;
class RevString {
	String revString (String str){
		Stack<Character> s = new Stack<Character>();
		for(int i=0;i<str.length();i++){
			s.push(str.charAt(i));
		}
		char stringArr[]=new char [str.length()];
		int i=0;
		while(!s.empty()){
			stringArr[i]=s.pop();
			i++;
		}
		return new String(stringArr);
	}
}
class Client {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter String to Reverse");
		String str = sc.nextLine();
		RevString obj = new RevString();
		String ret = obj.revString(str);
		System.out.println(ret);
	}
}

