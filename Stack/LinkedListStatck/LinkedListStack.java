// implimetation of stack using LinkedList

import java.util.*;
class LinkedListStack {
	Node top=null;
	class Node {
	int data;
	Node next=null;
	Node(int data){
		this.data=data;
	}
	}
	void push(int data){
		Node newNode=new Node(data);
		if(top==null)
			top=newNode;
		else{ 
			newNode.next=top;
			top=newNode;
			System.out.println("item inserted");
		}
	}
	int pop(){
		if(top==null)
			System.out.println("Stack underflow!");
		else{ 
		    int temp=top.data;
		    top=top.next;
		    return temp;
		}
		return 0;
	}

	void print(){
		if(top==null)
			System.out.println("Stack is empty");
		else{
			System.out.print("[ ");
			Node temp=top;
			while(temp!=null){
				System.out.print(temp.data);
			temp=temp.next;
			if(temp!=null)
				System.out.print(" , ");
			}
			System.out.print(" ]");
			System.out.println();
		}
	}
	int peek(){
		if(top==null)
			System.out.println("stack is empty");
		else 
			return top.data;
		return 0;
	}
	boolean isempty(){
		if(top==null)
			return true;
		return false;
	}
	int size(){
		int count=0;
		Node temp = top;
		while(temp!=null){
			count++;
			temp=temp.next;
		}
		return count;
	}

}
class Client {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		LinkedListStack s = new LinkedListStack();
		s.push(10);
		s.push(20);
		s.push(30);
		s.push(40);
		s.print();
		System.out.println(s.pop());
		s.print();
		System.out.println("peek element: "+s.peek());
		System.out.println("Stack is empty: "+s.isempty());
		System.out.println(s.size());
	}
}



	
