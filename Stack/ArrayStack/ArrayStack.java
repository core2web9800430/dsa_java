// implimetation of stack using array

import java.util.*;
class ArrayStack {
	int n=10;
	int arr[]=new int[n];
	int top=-1;
	void push(int data){
		if(top==n-1)
			System.out.println("stack is OverFlow!");
		else{
			top=top+1;
			arr[top]=data;
			System.out.println("item inserted");
		}
	}
	int pop(){
		if(top==-1)
			System.out.println("Stack underflow!");
		else{
			int temp=0;
			temp=arr[top];
			top--;
			return temp;
		}
		return 0;
	}

	void print(){
		if(top==-1)
			System.out.println("Stack is empty");
		else{
			System.out.print("[ ");
			for(int i=0;i<=top;i++){
				System.out.print(arr[i]);
				if(i<top)
					System.out.print(" , ");
			}
			System.out.print(" ]");
			System.out.println();
		}
	}
	int peek(){
		if(top==-1)
			System.out.println("stack is empty");
		else 
			return arr[top];
		return 0;
	}
	boolean isempty(){
		if(top==-1)
			return true;
		return false;
	}
}
class Client {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		ArrayStack s = new ArrayStack();
		s.push(10);
		s.push(20);
		s.push(30);
		s.push(40);
		s.print();
		System.out.println(s.pop());
		s.print();
		System.out.println("peek element: "+s.peek());
		System.out.println("Stack is empty: "+s.isempty());
	}
}



	
