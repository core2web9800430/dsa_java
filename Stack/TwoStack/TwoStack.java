// implimentation of two Stack in array

  import java.util.*;
  class TwoStackArray {
	  int arr[];
	  int top1;
	  int top2;
	  int size;
	  TwoStackArray(int size){
		  this.arr=new int[size];
		  this.top1=-1;
		  this.top2=size;
		  this.size=size;
	  }
	  void push1(int data){
		  if(top2-top1>1){ 
			  top1++;
			  arr[top1]=data;
		} else
		   System.out.println("Stack Overflow! "); 
	  }
	  void push2(int data){
		  if(top2-top1>1){
			  top2--;
			  arr[top2]=data;
		  }else
		   System.out.println("Stack Overflow! "); 
	  }
	  int pop1(){
		  if(top1==-1)
			  System.out.println("Stack Underflow!");
		  else{
			  int temp=arr[top1];
			  top1--;
			  return temp;
		  }
		  return 0;
	  }
	  int pop2(){
		  if(top2==size)
			  System.out.println("Stack Underflow!");
		  else{
			  int temp=arr[top2];
			  top2++;
			  return temp;
		  }
		  return 0;
	  }
	  void print1(){
		  if(top1==-1)
			  System.out.println("Stack is empty");
		  else {
			  System.out.print("[ ");
			  int i=0;
			  while(i<=top1){
				  System.out.print(arr[i]);
				  i++;
				  if(i<=top1)
					  System.out.print(" , ");
			  }
			  System.out.print(" ]");
			  System.out.println();
		  }
	  }
	  void print2(){
		  if(top2==size)
			  System.out.println("Stack is empty");
		  else { 
			  System.out.print("[ ");
			  int i= top2;
			  while(i<size){
				  System.out.print(arr[i]);
				  i++;
				  if(i<size)
					  System.out.print(" , ");
			  } 
			  System.out.print(" ]");
			  System.out.println();
		  }
	  }
  }
class Client {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of an Array");
		int size = sc.nextInt();
		TwoStackArray s = new TwoStackArray(size);
		char ch;
		do{
			System.out.println("1-push1");
			System.out.println("2-push2");
			System.out.println("3-pop1");
			System.out.println("4-pop2");
			System.out.println("5-print1");
			System.out.println("6-print2");
			System.out.println("....Enter your choice");

			int choice = sc.nextInt();
			switch(choice){
				case 1:{
					       System.out.println("Enter the data");
					       int data=sc.nextInt();
					       s.push1(data);
				}break;
				case 2:{
					       System.out.println("Enter the data");
					       int data=sc.nextInt();
					       s.push2(data);
				}break;
				case 3:{
					       System.out.println(s.pop1());
				}break;
				case 4:{
					       System.out.println(s.pop2());
				}break;
				case 5:{
					      s.print1();
				}break;
				case 6:{
					       s.print2();
				}break;
				default:{
					System.out.println("Invalid choice");
				}break;
			}
			System.out.println("..... if continue press 'y'");
			ch = sc.next().charAt(0);
		}while(ch=='Y' || ch=='y');
	}
}

