// check valid paranthesis
 import java.util.*;
 class ValidParanthesis {
	 boolean isValid(String str){
		 Stack<Character> s = new Stack<Character>();
		 for(int i=0;i<str.length();i++){
			 char ch = str.charAt(i);
			 if(ch=='{' || ch== '[' || ch == '('){
				 s.push(ch);
			 } else {
				 if(s.empty())
					 return false;
				 else{
					 char x = s.peek();
					 if((x=='('&& ch==')')||(x=='[' && ch==']') || (x=='{' && ch=='}'))
						 s.pop();
					 else
						 return false;
				 }
			 }
		 }
		 return true;
	 }
	 public static void main(String[] srgs){
	 Scanner sc = new Scanner(System.in);
	 ValidParanthesis vp = new ValidParanthesis();
	 System.out.println("Enter the expresion string of  paranthesis");
	 String str = sc.next();
	 boolean check = vp.isValid(str);
	 if(check)
	 System.out.println("valid expresion of paranthesis");
	 else
		 System.out.println("Invalid expresion of paranthesis");
	 }
 }

