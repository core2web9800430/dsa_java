// linear search algorithm using recursive call

import java.util.*;
class LinearSearch {
	public static void main (String []args){
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the size of an Array:");
		int n = sc.nextInt();
		int arr[] = new int[n];
		System.out.println("Enter the Elements in array:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter the Element to be Search:");
		int num= sc.nextInt();
	        
	         LinearSearch obj= new LinearSearch();
	        boolean check = obj.search(arr,num,n-1);	 

		if(check)
			System.out.println("Element found ");
		else
			System.out.println("Not Element found");
	} 
	boolean search(int arr[],int num,int n){
		if(n<0)
			return false;
	          if(arr[n] == num)
			  return true;
		  return search(arr,num,--n);
	} 
}



