// implimetation of Circular Queue using LinkedList

 import java.util.*;
 class Queue {
	 Node head =null;
	 Node tail = null;
	 class Node {
		 int data ;
		 Node next=null;
		 Node (int data){
			 this.data=data;
		 }
	 }
	 void enqueue(int data){
		 Node newNode = new Node(data);
		 if(head==null){
			 head=tail=newNode;
			 newNode.next=head;
		 } else{
			 tail.next=newNode;
			 tail=newNode;
			 tail.next=head;
		 }
	 }
	 int dequeue(){
		 if(head==null){
			 System.out.println("Queue is Empty");
			 return -1;
		 } else {
			 int ret = head.data;
			 if(head==tail){
				 head=tail=null;
			 } else{
				 head=head.next;
				 tail.next=head;
			 }
			 return ret;
		 }
	 }
	 void print(){
		 if(head==null){
			 System.out.println("Queue is empty");
			 return;
		 } else{
			 Node temp = head ;
			 while(temp.next!=head){
				 System.out.print(temp.data+" ");
				 temp=temp.next;
			 }
			 System.out.print(temp.data+" ");
			 System.out.println();
		 }
	 }
 }

class Client {
	public static void main(String [] pravin){
		Scanner sc = new Scanner(System.in);
		char ch;
		Queue q = new Queue();
		do{
			System.out.println("1-enqueue");
			System.out.println("2-dequeue");
			System.out.println("3-print ");

			System.out.println("...Enter your choise ");
			int choice =sc.nextInt();
			switch(choice){
				case 1:{
					       System.out.println("Enter the data");
					       int data=sc.nextInt();
				       	       q.enqueue(data);
				} break;
				case 2:{
					       int ret =q.dequeue();
					       if(ret!=-1)
						       System.out.println(ret+" poped");
				} break;
				case 3:{
					       q.print();
				}break;
				default:
				       System.out.println("Invalid choise");
				       break;
			}

			System.out.println("do you continue ? ");
			 ch = sc.next().charAt(0);
		}while(ch=='y'|| ch=='Y');
	}
}



