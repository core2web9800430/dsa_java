// Implimentation of Queue using LinkedList 

import java.util.*;
class Queue {
	Node head =null;
	Node tail = null;
	class Node {
		int data;
		Node next=null;
		Node(int data){
			this.data=data;
		}
	}
	void enqueue(int data){
		Node newNode = new Node(data);
		if(head==null)
			head=tail=newNode;
		else{
			tail.next=newNode;
			tail=newNode;
		}
	}
	int diqueue(){
		if(head==null){
			System.out.println("Queue is empty");
			return -1;
		} else {
			int temp=head.data;
			head= head.next;
			return temp;
		}
	}
	int peek(){
		if(head==null){
			System.out.println(" Queue is empty");
			return -1;
		} else {
			return head.data;
		}
	}
	boolean empty(){
		if(head==null)
			return true;
		else 
			return false;
	}
	void print(){
		if(head==null)
			System.out.println("Queue is Empty");
		else {
			Node temp = head;
			while(temp!=null){
				System.out.print(temp.data+" ");
				temp=temp.next;
			}
		}
	}
}
class Client {
	public static void main(String [] args ){
		Scanner sc = new Scanner (System.in);
		char ch;
		Queue q = new Queue();
		do{
			System.out.println("............................");
			System.out.println("1-add element");
			System.out.println("2-pop element");
			System.out.println("3-peek element");
			System.out.println("4-is Empty ");
			System.out.println("5- print ");
			
			System.out.println("....Enter your choice");
			int choice = sc.nextInt();
			switch(choice){
				case 1:{
					       System.out.println("Enter the element");
					       int data = sc.nextInt();
					       q.enqueue(data);
				} break;
				case 2:{
					       int ret = q.diqueue();
					       if(ret!=-1)
						       System.out.println(ret+" is poped");
				} break;
				case 3:{
					       int ret = q.peek();
					       if(ret!=-1)
						       System.out.println(ret+" is peek element");
				} break;
				case 4:{
					       if(q.empty())
						       System.out.println("Queue is empty");
					       else 
						       System.out.println("Queue is not empty");
				} break;
				case 5:{
					       q.print();
				} break;
		        	default:{
						System.out.println("Invalid choice");
				}
			}
			System.out.println(" ..Do you Continue ? ");
			ch = sc.next().charAt(0);
		}while(ch=='y' || ch=='Y');
	}
}
