//  implimentation of Circular Queue using Array

import java.util.*;
class Queue {
	int first,rear,maxsize;
	int arr[];
	Queue(int size){
		this.arr = new int[size];
		this.first=-1;
		this.rear=-1;
		this.maxsize=size;
	}
	void enqueue(int data){
		if((rear==maxsize-1 && first==0) || ((rear+1)%maxsize==first)){
			System.out.println("Queue is full");
			return;
		} else if((rear==maxsize-1) && (first!=0))
			rear =0;
		else if(rear==-1)
			rear=first=0;
		else
			rear++;
		arr[rear]=data;
	}
	int dequeue(){
		if(rear==-1){
			System.out.println("Queue is Empty");
			return -1;
		} else {
			int ret = arr[first];
			if(first==rear){
				first=rear=-1;
			} else if( first==maxsize-1){
				first=0;
			} else {
				first++;
			}
			return ret;
		}
	}
	void print(){
		System.out.println(first+"first , rear = "+rear);
		if(first==-1){
			System.out.println("Queue is empty");
			return;
		} else{ 
			if(first<=rear){
				for(int i= first;i<=rear;i++){
					System.out.print(arr[i]+" ");
				}
			} else{
				for(int i=first;i<=maxsize-1;i++){
				System.out.print(arr[i]+" ");
				}
				for(int i=0;i<=rear;i++){
					System.out.print(arr[i]+" ");
				}
			} 
			System.out.println();
		}
	}
}
class Client {
	public static void main(String [] args){
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the size of Array");
		int size = sc.nextInt();
		Queue q = new Queue(size);
		char ch;
		do{
			System.out.println(" 1-enqueue");
			System.out.println(" 2-dequeue");
			System.out.println(" 3-print");

			System.out.println(" .....Enter your choice ");
			int choice = sc.nextInt();
			switch(choice){
				case 1: {
						System.out.println("Enter the data ");
						int data = sc.nextInt();
						q.enqueue(data);
				} break;
				case 2:{
					       int ret = q.dequeue();
					       if(ret!=-1)
						       System.out.println(ret+" poped");
				} break;
				 case 3:{
						q.print();
				 }break;
				default:{
						System.out.println("Invalid choice");
				}
			} 
			System.out.println(" do you continue ?");
			ch = sc.next().charAt(0);
		}while(ch == 'Y' || ch=='y');
	}
}

