// implimentation of Queue using Array

import java.util.*;
class Queue {
	int first;
	int rear;
	int maxSize;
	int arr[];
	Queue(int size){
		this.arr = new int[size];
		this.rear=-1;
		this.first=-1;
		this.maxSize=size;
	}
	void enqueue(int data){
		if(rear==maxSize-1){
			System.out.println("Queue is full");
			return;
		}else if(rear==-1){
			first=rear=0;	
			arr[rear]=data;
		}else{
			rear++;
			arr[rear]=data;
		}
	}
	int diqueue(){
		if(first==-1){
			System.out.println("Queue is Empty");
			return-1;
		}
		int temp = arr[first];
		first++;
		if(first>rear)
			first=rear=-1;
		return temp;
	}
	int peek(){
		if(first==-1){
			System.out.println("Queue is empty");
			return -1;
		}
		return arr[first];
	}
	void print(){
		if(rear==-1)
			System.out.println("Queue is empty");
	      else{
	      	      for(int i=first;i<=rear;i++){
			System.out.print(arr[i]+" ");
		      }
		System.out.println();
	      }
	}
	boolean empty(){
		if(rear==-1)
			return true;
		else 
			return false;
	}  
}
class Client {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of Array");
		int size = sc.nextInt();
		Queue q = new Queue(size);
		char ch;
		do {
			System.out.println(".............................");
			System.out.println("1-add element");
			System.out.println("2-pop element");
			System.out.println("3-peek element");
			System.out.println("4-isEmpty ");
			System.out.println("5-print Queue ");

			System.out.println("....Enter choice....");
			int choice = sc.nextInt();

			switch(choice){
				case 1: {
						System.out.println("Enter the data");
						int data = sc.nextInt();
						q.enqueue(data);
				} break;
				case 2: {
						int ret= q.diqueue();
						if(ret!=-1)
							System.out.println(ret+" is poped");
				} break;
				case 3: {
						int ret= q.peek();
						if(ret!=-1)
							System.out.println(ret+" is peek elemrnt");
				} break;
				case 4 :{
						if(q.empty())
							System.out.println("Queue is Empty");
						else 
							System.out.println("Queue is not empty");
				} break;
				case 5 :{
						q.print();
				} break;
				default:{
						System.out.println("Invalid choice");
				}
			} 

			System.out.println(" Do you Continue ? ");
			ch = sc.next().charAt(0);
		} while(ch=='y' || ch=='Y');
	}
}

